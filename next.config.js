/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  images: {
    remotePatterns: [
      {
        protocol: "http",
        hostname: "192.168.1.25",
        port: "8000",
      },
      {
        protocol: "https",
        hostname: "sensible-adequately-troll.ngrok-free.app",
        port: "8000",
      },
    ],
  },

  redirects: async () => {
    return [
      {
        source: "/",
        destination: "/authentication/login",
        permanent: true,
      },
    ];
  },
};

module.exports = nextConfig;
