import axios from "axios";
import { congigObject } from "../../src/constants/constants";
import { authService } from "../../src/services/authService";
import { toast } from "react-toastify";

const server = axios.create({
  baseURL: `${congigObject.baseUrl}`,
});
let refresh = false;

server.interceptors.response.use(
  (resp) => {
    return resp;
  },
  async (error) => {
    if (typeof window === undefined) return;
    if (error.response.status === 400) {
      toast.error(error.response.data.message);
    }
    if (error.response.status === 409) {
      toast.error(error.response.data.message);
    }
    if (error.response.status === 401 && !refresh) {
      refresh = true;
      const refreshToken = localStorage.getItem("userRefreshToken");
      const response = await authService.RefreshToken({ refreshToken });
      if (!response) {
        return;
      }
      if (response.status === 200) {
        const accessToken = response.data.accessToken;
        const refreshToken = response.data.refreshToken;
        localStorage.setItem("userRefreshToken", refreshToken);
        localStorage.setItem("userAccess", `Bearer ${accessToken}`);
        axios.defaults.headers.common[
          "Authorization"
        ] = `Bearer ${response.data.accessToken["token"]}`;
        return axios(error.config);
      }
    }
    refresh = false;
    return error;
  }
);

export default server;
