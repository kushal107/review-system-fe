const validate = (values) => {
  let errors: any = {};

  if (!values.email) {
    errors.email = "Email address is required";
  } else if (
    !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(values.email)
  ) {
    errors.email = "Email address is invalid";
  }

  if (!values.password?.trim()) {
    errors.password = "Password is required";
  } else if (values.password.length < 8) {
    errors.password = "Password must be 8 or more characters";
  }

  return errors;
};

export default validate;
