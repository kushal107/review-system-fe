import validateSignIn from "./loginValidation";
import validateSignUp from "./signupValidation";
import validateEditProfile from "./editProfileValidation";
export { validateSignIn, validateSignUp, validateEditProfile };
