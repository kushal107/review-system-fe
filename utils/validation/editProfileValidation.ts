const validate = (values) => {
  let errors: any = {};

  if (!values.fullName) {
    errors.fullName = "Name is required";
  }

  if (!values.email) {
    errors.email = "Email address is required";
  } else if (
    !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(values.email)
  ) {
    errors.email = "Email address is invalid";
  }

  if (!values.address) {
    errors.address = "Address is required";
  }

  if (!values.pinCode) {
    errors.pinCode = "Pincode is required";
  }

  if (!values.country) {
    errors.country = "Country is required";
  }

  if (!values.city) {
    errors.city = "City is required";
  }

  if (!values.phoneNo) {
    errors.phoneNo = "Phone is required";
  } else if (values.phoneNo && values.phoneNo.length != 10) {
    errors.phoneNo = "Phone is invalid";
  }

  return errors;
};

export default validate;
