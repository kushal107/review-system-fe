const TOKEN_KEY = "userAccess";
const RefreshToken = "userRefreshToken";
const USER_TOKEN_KEY = "userRefreshTokenTemp";

const getToken = () => {
  if (typeof window !== "undefined") {
    const token = localStorage.getItem(TOKEN_KEY);
    if (token) {
      return token; // Return the token if it exists
    }
  }
  return false; // Return false if window is undefined or token doesn't exist
};
const getuserToken = () => {
  if (typeof window !== "undefined") {
    const token = localStorage.getItem(USER_TOKEN_KEY);
    if (!token) {
      localStorage.removeItem(USER_TOKEN_KEY);
      return false;
    }
    return token;
  }
};

const setToken = (tokenValue) => {
  if (typeof window !== "undefined") {
    localStorage.setItem(TOKEN_KEY, tokenValue);
    return true;
  }
};

const removeToken = () => {
  if (typeof window !== "undefined") {
    localStorage.removeItem(TOKEN_KEY);
    localStorage.removeItem(RefreshToken);
    localStorage.clear();
    return true;
  }
};
const setRefreshToken = (tokenValue) => {
  if (typeof window !== "undefined") {
    localStorage.setItem(RefreshToken, tokenValue);
    return true;
  }
};
const getRefreshToken = () => {
  if (typeof window !== "undefined") {
    const token = localStorage.getItem(RefreshToken);
    if (!token) {
      localStorage.removeItem(RefreshToken);
      return false;
    }
    return token;
  }
};

export {
  getToken,
  getuserToken,
  setToken,
  removeToken,
  setRefreshToken,
  getRefreshToken,
};
