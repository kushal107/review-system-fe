export const congigObject = {
  port: 3000,

  baseUrl: "https://sensible-adequately-troll.ngrok-free.app/api",
  // baseUrl: "http://192.168.1.25:8000/api",


  //
  filesFormats: [
    ".jpeg",
    ".jpg",
    ".png",
    "image/png",
    "image/jpg",
    "image/jpeg",
    ".JPEG",
    ".JPG",
    ".PNG",
    "image/PNG",
    "image/JPG",
    "image/JPEG",
  ],
  vedioUpload: [".MP4", ".mp4", ".MKV", ".mkv"],
  docUploads: [
    ".ppt",
    ".pptx",
    ".doc",
    ".docx",
    ".pdf",
    ".PPT",
    ".PPTX",
    ".DOC",
    ".DOCX",
    ".PDF",
  ],
  DEV: typeof window !== "undefined" ? document.domain === "localhost" : "",
};
