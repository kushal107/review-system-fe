import type { NextRequest } from "next/server";
import { NextResponse } from "next/server";

export default function middleware(request: NextRequest) {
  const path = request.nextUrl.pathname;

  if (
    path.split("/")[1] !== "authentication" &&
    !request.cookies.has("userAccess")
  ) {
    return NextResponse.redirect(new URL("/authentication/login", request.url));
  }
  if (
    path.split("/")[1] === "authentication" &&
    request.cookies.has("userAccess")
  ) {
    return NextResponse.redirect(new URL(`/dashboard`, request.url));
  }

  if (
    path.split("/")[1] !== "authentication" &&
    request.cookies.has("userAccess")
  ) {
    return NextResponse.next();
  }

  return NextResponse.next();
}

export const config = {
  matcher: [
    "/",
    "/dashboard/:path*",
    "/restaurants/:path*",
    "/authentication/:path*",
  ],
};
