import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { deleteRequest, getRequest, postRequest, putRequest } from "..";

const PLATFORM_URL = "/platform";

const getReviewPlatform = async (queryParams) => {
  try {
    const Path = `${PLATFORM_URL}`;
    return await getRequest(Path);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

const getReviewPlatformById = async (id) => {
  try {
    return await getRequest(PLATFORM_URL + "/" + id);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

const addReviewPlatformProfile = async (payload) => {
  try {
    const Path = PLATFORM_URL;
    return await postRequest(Path, payload);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

const updateReviewPlatformProfile = async (payload, id) => {
  try {
    const Path = PLATFORM_URL + "/" + id;
    return await putRequest(Path, payload);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

const deleteReviewPlatform = async (id) => {
  try {
    const Path = PLATFORM_URL + "/" + id;
    return await deleteRequest(Path);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

export const reviewPlatformService = {
  getReviewPlatform,
  getReviewPlatformById,
  updateReviewPlatformProfile,
  deleteReviewPlatform,
  addReviewPlatformProfile,
};
