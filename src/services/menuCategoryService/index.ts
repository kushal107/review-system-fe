import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  deleteRequest,
  getRequest,
  postRequest,
  postReqWithoutToken,
  postReqWithoutToken1,
  putRequest,
} from "..";

const MENU_URL = "/menu-category";

const getMenuCategory = async (queryParams) => {
  try {
    const { search, pageRecord, pageNo, sortBy, order, isApproved, userId } =
      queryParams;
    const Path = `${MENU_URL}/list?search=${search}&pageRecord=${pageRecord}&pageNo=${pageNo}&sortBy=${sortBy}&order=${order}&isApproved=${isApproved}&userId=${userId}`;
    return await getRequest(Path);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

const getMenuCategoryById = async (id) => {
  try {
    return await getRequest(MENU_URL + "/" + id);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

const addMenuCategory = async (payload) => {
  try {
    const Path = "/menu-category";
    return await postRequest(Path, payload);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

const updateMenuCategory = async (payload,id) => {
  try {
    const Path = "/menu-category/" + id;
    return await putRequest(Path, payload);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

const deleteMenuCategory = async (id) => {
  try {
    const Path = MENU_URL + "/" + id;
    return await deleteRequest(Path);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

export const publishMenuCategoryData = async (id) => {
  try {
    const Path = MENU_URL + "/toggle-publish/" + id;
    return await putRequest(Path, {});
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

export const menuCategoryService = {
  getMenuCategory,
  getMenuCategoryById,
  addMenuCategory,
  updateMenuCategory,
  deleteMenuCategory,
  publishMenuCategoryData,
};
