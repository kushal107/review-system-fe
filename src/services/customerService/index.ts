import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  deleteRequest,
  getRequest,
  postRequest,
  postReqWithoutToken,
  postReqWithoutToken1,
  putRequest,
} from "..";

const CUSTOMER_URL = "/customer";

const getCustomer = async (queryParams) => {
  try {
    const { search, pageRecord, pageNo, sortBy, order } =
      queryParams;
    const Path = `${CUSTOMER_URL}/list?search=${search}&pageRecord=${pageRecord}&pageNo=${pageNo}&sortBy=${sortBy}&order=${order}`;
    return await getRequest(Path);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

const getCustomerById = async (id) => {
  try {
    return await getRequest(CUSTOMER_URL + "/" + id);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

const addCustomer = async (payload) => {
  try {
    const Path = CUSTOMER_URL;
    return await postRequest(Path, payload);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

const updateCustomer = async (payload,id) => {
  try {
    const Path = CUSTOMER_URL + id;
    return await putRequest(Path, payload);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

const deleteCustomer = async (id) => {
  try {
    const Path = CUSTOMER_URL + "/" + id;
    return await deleteRequest(Path);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

export const customerService = {
  getCustomer,
  getCustomerById,
  addCustomer,
  updateCustomer,
  deleteCustomer
};
