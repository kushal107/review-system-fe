import { congigObject } from "../constants/constants";
import server from "../../utils/interceptors/interceptors";
import { getToken } from "../../utils/localstorage/token";
import { getuserToken } from "../../utils/localstorage/token";
// Handling GET request
const USER_TOKEN = getToken();

const getRequestAdmin = async (path) => {
  const AXIOS_CONFIG = {
    headers: {
      "Access-Control-Allow-Origin": "*",
      Authorization: USER_TOKEN,
    },
  };

  const API_ENDPOINT = `${congigObject.baseUrl}${path}`;
  return await server.get(API_ENDPOINT, AXIOS_CONFIG);
};

const postReqWithoutToken1 = async (path, payload) => {
  const USER_TOKEN = getToken();
  const AXIOS_CONFIG = {
    headers: {
      "Access-Control-Allow-Origin": "*",
      Authorization: USER_TOKEN,
    },
  };
  const API_ENDPOINT = `${congigObject.baseUrl}${path}`;
  return await server.post(API_ENDPOINT, payload, AXIOS_CONFIG);
};

const getRequest = async (path) => {
  const USER_TOKEN = getToken();
  const AXIOS_CONFIG = {
    headers: {
      "Access-Control-Allow-Origin": "*",
      Authorization: USER_TOKEN,
    },
  };
  const API_ENDPOINT = `${congigObject.baseUrl}${path}`;
  return await server.get(API_ENDPOINT, AXIOS_CONFIG);
};

const getReqWithoutToken = async (path) => {
  const AXIOS_CONFIG = {
    headers: {
      "Access-Control-Allow-Origin": "*",
    },
  };
  const API_ENDPOINT = `${congigObject.baseUrl}${path}`;
  return await server.get(API_ENDPOINT, AXIOS_CONFIG);
};

const getReqWithoutTokens = async (path) => {
  // const AXIOS_CONFIG = {
  //   headers: {
  //     "Access-Control-Allow-Origin": "*",
  //   }
  // };
  const API_ENDPOINT = `${path}`;
  return await server.get(API_ENDPOINT);
};

// Handling POST request
const postRequest = async (path, payload) => {
  const USER_TOKEN = getToken();
  const AXIOS_CONFIG = {
    headers: {
      "Access-Control-Allow-Origin": "*",
      Authorization: USER_TOKEN,
    },
  };
  const API_ENDPOINT = `${congigObject.baseUrl}${path}`;
  return await server.post(API_ENDPOINT, payload, AXIOS_CONFIG);
};

const postReqWithFormData = async (path, formData) => {
  const USER_TOKEN = getToken();
  const AXIOS_CONFIG = {
    headers: {
      "Access-Control-Allow-Origin": "*",
      Authorization: USER_TOKEN,
    },
  };
  const API_ENDPOINT = `${congigObject.baseUrl}${path}`;
  return await server.post(API_ENDPOINT, formData, AXIOS_CONFIG);
};

const postReqWithoutToken = async (path, payload) => {
  const AXIOS_CONFIG = {
    headers: {
      "Access-Control-Allow-Origin": "*",
    },
  };
  const API_ENDPOINT = `${congigObject.baseUrl}${path}`;
  return await server.post(API_ENDPOINT, payload, AXIOS_CONFIG);
};

// Handling PUT request
const putRequest = async (path, payload) => {
  const USER_TOKEN = getToken();
  const AXIOS_CONFIG = {
    headers: {
      "Access-Control-Allow-Origin": "*",
      Authorization: USER_TOKEN,
    },
  };
  const API_ENDPOINT = `${congigObject.baseUrl}${path}`;
  return await server.put(API_ENDPOINT, payload, AXIOS_CONFIG);
};

const putReqWithFormData = async (path, formData) => {
  const USER_TOKEN = getToken();
  const AXIOS_CONFIG = {
    headers: {
      "Access-Control-Allow-Origin": "*",
      Authorization: USER_TOKEN,
    },
  };
  const API_ENDPOINT = `${congigObject.baseUrl}${path}`;
  return await server.put(API_ENDPOINT, formData, AXIOS_CONFIG);
};

// Handling DELETE request
const deleteRequest = async (path) => {
  const USER_TOKEN = getToken();
  const AXIOS_CONFIG = {
    headers: {
      "Access-Control-Allow-Origin": "*",
      Authorization: USER_TOKEN,
    },
  };
  const API_ENDPOINT = `${congigObject.baseUrl}${path}`;
  return await server.delete(API_ENDPOINT, AXIOS_CONFIG);
};

export {
  getRequest,
  getReqWithoutToken,
  postRequest,
  postReqWithFormData,
  postReqWithoutToken,
  putRequest,
  putReqWithFormData,
  deleteRequest,
  getReqWithoutTokens,
  getRequestAdmin,
  postReqWithoutToken1,
};
