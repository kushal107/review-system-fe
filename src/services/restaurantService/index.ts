import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  deleteRequest,
  getRequest,
  postRequest,
  postReqWithoutToken,
  postReqWithoutToken1,
  putRequest,
} from "..";

const RESTRAURANT_URL = "/restaurant";

const getRestaurant = async (queryParams) => {
  try {
    const { search, pageRecord, pageNo, sortBy, order, isApproved } =
      queryParams;
    const Path = `${RESTRAURANT_URL}/list?search=${search}&pageRecord=${pageRecord}&pageNo=${pageNo}&sortBy=${sortBy}&order=${order}&isApproved=${isApproved}`;
    return await getRequest(Path);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

const getRestaurantById = async (id) => {
  try {
    return await getRequest(RESTRAURANT_URL + "/" + id);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

const updateRestaurantProfile = async (payload) => {
  try {
    const Path = "/restaurant-profile";
    return await putRequest(Path, payload);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

const deleteRestaurant = async (id) => {
  try {
    const Path = RESTRAURANT_URL + "/" + id;
    return await deleteRequest(Path);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

export const publishRestaurantData = async (id) => {
  try {
    const Path = RESTRAURANT_URL + "/toggle-publish/" + id;
    return await putRequest(Path, {});
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

export const restaurantService = {
  getRestaurant,
  getRestaurantById,
  updateRestaurantProfile,
  deleteRestaurant,
  publishRestaurantData,
};
