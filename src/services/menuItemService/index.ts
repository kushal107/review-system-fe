import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  deleteRequest,
  getRequest,
  postRequest,
  postReqWithoutToken,
  postReqWithoutToken1,
  putRequest,
} from "..";

const MENU_URL = "/menu";

const getMenuItem = async (queryParams) => {
  try {
    const { search, pageRecord, pageNo, sortBy, order, isApproved, menuCategoryId } =
      queryParams;
    const Path = `${MENU_URL}/list?search=${search}&pageRecord=${pageRecord}&pageNo=${pageNo}&sortBy=${sortBy}&order=${order}&isApproved=${isApproved}&menuCategoryId=${menuCategoryId}`;
    return await getRequest(Path);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

const getMenuItemById = async (id) => {
  try {
    return await getRequest(MENU_URL + "/" + id);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

const addMenuItem = async (payload) => {
  try {
    const Path = MENU_URL;
    return await postRequest(Path, payload);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

const updateMenuItem = async (payload,id) => {
  try {
    const Path = MENU_URL + "/" + id;
    return await putRequest(Path, payload);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

const deleteMenuItem = async (id) => {
  try {
    const Path = MENU_URL + "/" + id;
    return await deleteRequest(Path);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

export const publishMenuItemData = async (id) => {
  try {
    const Path = MENU_URL + "/toggle-publish/" + id;
    return await putRequest(Path, {});
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

export const menuItemService = {
  getMenuItem,
  getMenuItemById,
  addMenuItem,
  updateMenuItem,
  deleteMenuItem,
  publishMenuItemData,
};
