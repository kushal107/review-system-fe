import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { deleteRequest, getRequest, putRequest } from "..";

const REVIEW_URL = "/review";

const getReview = async (queryParams) => {
  try {
    const { search, pageRecord, pageNo, sortBy, order, restaurantId } =
      queryParams;
    const Path = `${REVIEW_URL}/${restaurantId}?search=${search}&pageRecord=${pageRecord}&pageNo=${pageNo}&sortBy=${sortBy}&order=${order}`;
    return await getRequest(Path);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

const getReviewById = async (id) => {
  try {
    return await getRequest(REVIEW_URL + "/" + id);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

const deleteReview = async (id) => {
  try {
    const Path = REVIEW_URL + "/" + id;
    return await deleteRequest(Path);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

export const reviewService = {
  getReview,
  getReviewById,
  deleteReview,
};
