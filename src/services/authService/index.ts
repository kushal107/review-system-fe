import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  getRequest,
  postRequest,
  postReqWithoutToken,
  postReqWithoutToken1,
} from "..";
const AUTH_URL = "/auth";
const RESTRAURANT_URL = "/restaurant";
const SignUp = async (payload) => {
  try {
    const signUpPath = `${AUTH_URL}/signup`;
    return await postReqWithoutToken(signUpPath, payload);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

const SignIn = async (payload) => {
  try {
    const signInPath = `${AUTH_URL}/login`;
    return await postReqWithoutToken(signInPath, payload);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

const RefreshToken = async (payload) => {
  try {
    const refreshInPath = `${AUTH_URL}/refresh-token`;
    return await postReqWithoutToken(refreshInPath, payload);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

const GetUser = async () => {
  try {
    const userPath = `${RESTRAURANT_URL}`;
    return await getRequest(userPath);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

const ForgetPasswordService = async (payload) => {
  try {
    const Path = `${AUTH_URL}/forgot-password`;
    return await postReqWithoutToken(Path, payload);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

const ResetPasswordService = async (payload) => {
  try {
    const Path = `${AUTH_URL}/reset-password`;
    return await postReqWithoutToken(Path, payload);
  } catch (error) {
    toast.error(error?.response?.data?.message);
  }
};

export const authService = {
  SignUp,
  SignIn,
  GetUser,
  RefreshToken,
  ForgetPasswordService,
  ResetPasswordService,
};
