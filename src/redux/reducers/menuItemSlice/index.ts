import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { menuItemService } from "../../../services/menuItemService";
const initialState = {
  menuItem: [],
  loading: false,
  error: "",
};

export const getMenuItem: any = createAsyncThunk(
  "getMenuItem",
  async (payload) => {
    const { data, status } = await menuItemService.getMenuItem(payload);
    data.status = status;
    return data;
  }
);
export const getMenuItemById: any = createAsyncThunk(
  "getMenuItemById",
  async (payload) => {
    const { data, status } = await menuItemService.getMenuItemById(
      payload
    );
    data.status = status;
    return data;
  }
);

export const addMenuItem: any = createAsyncThunk(
  "addMenuItem",
  async (payload: any) => {
    const { data, status } = await menuItemService.addMenuItem(
      payload.formData
    );
    data.status = status;
    return data;
  }
);

export const updateMenuItem: any = createAsyncThunk(
  "updateMenuItem",
  async (payload: any) => {
    const { data, status } = await menuItemService.updateMenuItem(
      payload.formData,
      payload.id
    );
    data.status = status;
    return data;
  }
);

export const deleteMenuItem: any = createAsyncThunk(
  "deleteMenuItem",
  async (payload) => {
    const { data, status } = await menuItemService.deleteMenuItem(
      payload
    );
    data.status = status;
    return data;
  }
);

export const publishMenuItemData: any = createAsyncThunk(
  "publishMenuItemData",
  async (payload) => {
    const { data, status } = await menuItemService.publishMenuItemData(
      payload
    );
    data.status = status;
    return data;
  }
);

const menuItemSlice = createSlice({
  name: "menuItem",
  initialState,
  reducers: {},
  extraReducers: {
    ///--------------getMenuItem-------

    [getMenuItem.pending]: (state, action) => {
      state.loading = true;
    },
    [getMenuItem.fulfilled]: (state, { payload }) => {
      state.loading = false;
      state.menuItem = payload;
    },
    [getMenuItem.rejected]: (state, { payload }) => {
      state.loading = false;
    },

    ///--------------getMenuItemById-------

    [getMenuItemById.pending]: (state, action) => {
      state.loading = true;
    },
    [getMenuItemById.fulfilled]: (state, { payload }) => {
      state.loading = false;
      state.menuItem = payload;
    },
    [getMenuItemById.rejected]: (state, { payload }) => {
      state.loading = false;
    },

    ///deleteMenuItem

    [deleteMenuItem.pending]: (state, action) => {
      state.loading = true;
    },
    [deleteMenuItem.fulfilled]: (state, { payload }) => {
      state.loading = false;
      state.menuItem = payload;
    },
    [deleteMenuItem.rejected]: (state, { payload }) => {
      state.loading = false;
    },

    /// -------------uupdateMenuItem-----------
    [updateMenuItem.pending]: (state, action) => {
      state.loading = true;
    },
    [updateMenuItem.fulfilled]: (state, { payload }) => {
      state.loading = false;
      state.menuItem = payload;
    },
    [updateMenuItem.rejected]: (state, { payload }) => {
      state.loading = false;
    },

    /// -------------addMenuItem-----------
    [addMenuItem.pending]: (state, action) => {
      state.loading = true;
    },
    [addMenuItem.fulfilled]: (state, { payload }) => {
      state.loading = false;
      state.menuItem = payload;
    },
    [addMenuItem.rejected]: (state, { payload }) => {
      state.loading = false;
    },

    /// -------------publish menuItem-----------
    [publishMenuItemData.pending]: (state, action) => {
      state.loading = true;
    },
    [publishMenuItemData.fulfilled]: (state, { payload }) => {
      state.loading = false;
      state.menuItem = payload;
    },
    [publishMenuItemData.rejected]: (state, { payload }) => {
      state.loading = false;
    },
  },
});

export default menuItemSlice.reducer;
