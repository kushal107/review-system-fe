import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { reviewPlatformService } from "../../../services/reviewPlatformService";
const initialState = {
  reviewPlatform: [],
  loading: false,
  error: "",
};

export const getReviewPlatform: any = createAsyncThunk(
  "getreviewPlatform",
  async (payload) => {
    const { data, status } = await reviewPlatformService.getReviewPlatform(
      payload
    );
    data.status = status;
    return data;
  }
);
export const getReviewPlatformById: any = createAsyncThunk(
  "getReviewPlatformById",
  async (payload) => {
    const { data, status } = await reviewPlatformService.getReviewPlatformById(
      payload
    );
    data.status = status;
    return data;
  }
);

export const addReviewPlatform: any = createAsyncThunk(
  "addReviewPlatform",
  async (payload: any) => {
    const { data, status } =
      await reviewPlatformService.addReviewPlatformProfile(payload.formData);
    data.status = status;
    return data;
  }
);

export const updateReviewPlatform: any = createAsyncThunk(
  "updateReviewPlatform",
  async (payload: any) => {
    const { data, status } =
      await reviewPlatformService.updateReviewPlatformProfile(
        payload.formData,
        payload.id
      );
    data.status = status;
    return data;
  }
);

export const deleteReviewPlatform: any = createAsyncThunk(
  "deleteReviewPlatform",
  async (payload) => {
    const { data, status } = await reviewPlatformService.deleteReviewPlatform(
      payload
    );
    data.status = status;
    return data;
  }
);

const reviewPlatformlice = createSlice({
  name: "reviewPlatform",
  initialState,
  reducers: {},
  extraReducers: {
    ///--------------getReviewPlatform-------

    [getReviewPlatform.pending]: (state, action) => {
      state.loading = true;
    },
    [getReviewPlatform.fulfilled]: (state, { payload }) => {
      state.loading = false;
      state.reviewPlatform = payload;
    },
    [getReviewPlatform.rejected]: (state, { payload }) => {
      state.loading = false;
    },

    ///--------------getReviewPlatformById-------

    [getReviewPlatformById.pending]: (state, action) => {
      state.loading = true;
    },
    [getReviewPlatformById.fulfilled]: (state, { payload }) => {
      state.loading = false;
      state.reviewPlatform = payload;
    },
    [getReviewPlatformById.rejected]: (state, { payload }) => {
      state.loading = false;
    },

    ///deleteReviewPlatform

    [deleteReviewPlatform.pending]: (state, action) => {
      state.loading = true;
    },
    [deleteReviewPlatform.fulfilled]: (state, { payload }) => {
      state.loading = false;
      state.reviewPlatform = payload;
    },
    [deleteReviewPlatform.rejected]: (state, { payload }) => {
      state.loading = false;
    },

    /// -------------addReviewPlatform-----------
    [addReviewPlatform.pending]: (state, action) => {
      state.loading = true;
    },
    [addReviewPlatform.fulfilled]: (state, { payload }) => {
      state.loading = false;
      state.reviewPlatform = payload;
    },
    [addReviewPlatform.rejected]: (state, { payload }) => {
      state.loading = false;
    },

    /// -------------uupdateReviewPlatform-----------
    [updateReviewPlatform.pending]: (state, action) => {
      state.loading = true;
    },
    [updateReviewPlatform.fulfilled]: (state, { payload }) => {
      state.loading = false;
      state.reviewPlatform = payload;
    },
    [updateReviewPlatform.rejected]: (state, { payload }) => {
      state.loading = false;
    },
  },
});

export default reviewPlatformlice.reducer;
