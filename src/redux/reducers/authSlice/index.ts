import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { authService } from "../../../services/authService";

const initialState = {
  user: {},
  loading: false,
};
export const signUpUser: any = createAsyncThunk("signUp", async (playload) => {
  const { data, status } = await authService.SignUp(playload);
  data.status = status;
  return data;
});

export const loginInUser: any = createAsyncThunk("signIn", async (playload) => {
  const { data, status } = await authService.SignIn(playload);
  data.status = status;
  return data;
});

export const fetchUser: any = createAsyncThunk(
  "singleUser",
  async () => {
    const { data, status } = await authService.GetUser();
    data.status = status;
    return data;
  }
);

export const ForgetPasswordReq: any = createAsyncThunk(
  "forgetPwd",
  async (payload) => {
    const { data, status } = await authService.ForgetPasswordService(payload);
    data.status = status;
    return data;
  }
);

export const ResetPasswordReq: any = createAsyncThunk(
  "resetPwd",
  async (payload) => {
    const { data, status } = await authService.ResetPasswordService(payload);
    data.status = status;
    return data;
  }
);

const AuthSlice = createSlice({
  name: "authUser",
  initialState,
  reducers: {
    logoutAuth: (state) => {
      state.user = {};
      state.loading = false;
    },
    setSignleUser: (state, { payload }) => {
      state.user = payload;
    },
  },
  extraReducers: {
    /// -------------signUpUser-----------
    [loginInUser.pending]: (state, action) => {
      state.loading = true;
    },
    [loginInUser.fulfilled]: (state: any, { payload }) => {
      state.loading = false;
      state.isSuccess = true;
      state.user = payload.user;
      state.message = payload.message;
    },
    [loginInUser.rejected]: (state: any, { payload }) => {
      state.loading = false;
      state.isSuccess = false;
      state.message = "failed";
    },

    /// -------------signUpUser-----------
    [signUpUser.pending]: (state, action) => {
      state.loading = true;
    },
    [signUpUser.fulfilled]: (state: any, { payload }) => {
      state.loading = false;
      state.isSuccess = true;
    },
    [signUpUser.rejected]: (state: any, { payload }) => {
      state.loading = false;
      state.isSuccess = false;
      state.message = "failed";
    },

    /// -------------fetchUser-----------
    [fetchUser.pending]: (state, action) => {
      state.loading = true;
    },
    [fetchUser.fulfilled]: (state: any, { payload }) => {
      state.loading = false;
      state.user = payload;
      state.isSuccess = true;
    },
    [fetchUser.rejected]: (state: any, { payload }) => {
      state.loading = false;
      state.isSuccess = false;
      state.message = "failed";
    },

    /// -------------forget Password-----------
    [ForgetPasswordReq.pending]: (state, action) => {
      state.loading = true;
    },
    [ForgetPasswordReq.fulfilled]: (state: any, { payload }) => {
      state.loading = false;
      state.isSuccess = true;
    },
    [ForgetPasswordReq.rejected]: (state: any, { payload }) => {
      state.loading = false;
      state.isSuccess = false;
      state.message = "failed";
    },

    ///=================ResetPasswordReq=====================
    [ResetPasswordReq.pending]: (state, action) => {
      state.loading = true;
    },
    [ResetPasswordReq.fulfilled]: (state: any, { payload }) => {
      state.loading = false;
      state.isSuccess = true;
    },
    [ResetPasswordReq.rejected]: (state: any, { payload }) => {
      state.loading = false;
      state.isSuccess = false;
      state.message = "failed";
    },
  },
});

export const { logoutAuth, setSignleUser } = AuthSlice.actions;
export default AuthSlice.reducer;
