import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { menuCategoryService } from "../../../services/menuCategoryService";
const initialState = {
  menuCategory: [],
  loading: false,
  error: "",
};

export const getMenuCategory: any = createAsyncThunk(
  "getMenuCategory",
  async (payload) => {
    const { data, status } = await menuCategoryService.getMenuCategory(payload);
    data.status = status;
    return data;
  }
);
export const getMenuCategoryById: any = createAsyncThunk(
  "getMenuCategoryById",
  async (payload) => {
    const { data, status } = await menuCategoryService.getMenuCategoryById(
      payload
    );
    data.status = status;
    return data;
  }
);

export const addMenuCategory: any = createAsyncThunk(
  "updateMenuCategory",
  async (payload: any) => {
    const { data, status } = await menuCategoryService.addMenuCategory(
      payload.formData
    );
    data.status = status;
    return data;
  }
);

export const updateMenuCategory: any = createAsyncThunk(
  "updateMenuCategory",
  async (payload: any) => {
    const { data, status } = await menuCategoryService.updateMenuCategory(
      payload.formData,
      payload.id
    );
    data.status = status;
    return data;
  }
);

export const deleteMenuCategory: any = createAsyncThunk(
  "deleteMenuCategory",
  async (payload) => {
    const { data, status } = await menuCategoryService.deleteMenuCategory(
      payload
    );
    data.status = status;
    return data;
  }
);

export const publishMenuCategoryData: any = createAsyncThunk(
  "publishMenuCategoryData",
  async (payload) => {
    const { data, status } = await menuCategoryService.publishMenuCategoryData(
      payload
    );
    data.status = status;
    return data;
  }
);

const menuCategorySlice = createSlice({
  name: "menuCategory",
  initialState,
  reducers: {},
  extraReducers: {
    ///--------------getMenuCategory-------

    [getMenuCategory.pending]: (state, action) => {
      state.loading = true;
    },
    [getMenuCategory.fulfilled]: (state, { payload }) => {
      state.loading = false;
      state.menuCategory = payload;
    },
    [getMenuCategory.rejected]: (state, { payload }) => {
      state.loading = false;
    },

    ///--------------getMenuCategoryById-------

    [getMenuCategoryById.pending]: (state, action) => {
      state.loading = true;
    },
    [getMenuCategoryById.fulfilled]: (state, { payload }) => {
      state.loading = false;
      state.menuCategory = payload;
    },
    [getMenuCategoryById.rejected]: (state, { payload }) => {
      state.loading = false;
    },

    ///deleteMenuCategory

    [deleteMenuCategory.pending]: (state, action) => {
      state.loading = true;
    },
    [deleteMenuCategory.fulfilled]: (state, { payload }) => {
      state.loading = false;
      state.menuCategory = payload;
    },
    [deleteMenuCategory.rejected]: (state, { payload }) => {
      state.loading = false;
    },

    /// -------------uupdateMenuCategory-----------
    [updateMenuCategory.pending]: (state, action) => {
      state.loading = true;
    },
    [updateMenuCategory.fulfilled]: (state, { payload }) => {
      state.loading = false;
      state.menuCategory = payload;
    },
    [updateMenuCategory.rejected]: (state, { payload }) => {
      state.loading = false;
    },

    /// -------------addMenuCategory-----------
    [addMenuCategory.pending]: (state, action) => {
      state.loading = true;
    },
    [addMenuCategory.fulfilled]: (state, { payload }) => {
      state.loading = false;
      state.menuCategory = payload;
    },
    [addMenuCategory.rejected]: (state, { payload }) => {
      state.loading = false;
    },

    /// -------------publish menuCategory-----------
    [publishMenuCategoryData.pending]: (state, action) => {
      state.loading = true;
    },
    [publishMenuCategoryData.fulfilled]: (state, { payload }) => {
      state.loading = false;
      state.menuCategory = payload;
    },
    [publishMenuCategoryData.rejected]: (state, { payload }) => {
      state.loading = false;
    },
  },
});

export default menuCategorySlice.reducer;
