import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { restaurantService } from "../../../services/restaurantService";
const initialState = {
  restaurant: [],
  loading: false,
  error: "",
};

export const getRestaurant: any = createAsyncThunk(
  "getrestaurant",
  async (payload) => {
    const { data, status } = await restaurantService.getRestaurant(payload);
    data.status = status;
    return data;
  }
);
export const getRestaurantById: any = createAsyncThunk(
  "getRestaurantById",
  async (payload) => {
    const { data, status } = await restaurantService.getRestaurantById(payload);
    data.status = status;
    return data;
  }
);

export const updateRestaurant: any = createAsyncThunk(
  "updateRestaurant",
  async (payload: any) => {
    const { data, status } = await restaurantService.updateRestaurantProfile(
      payload.formData
    );
    data.status = status;
    return data;
  }
);

export const deleteRestaurant: any = createAsyncThunk(
  "deleteRestaurant",
  async (payload) => {
    const { data, status } = await restaurantService.deleteRestaurant(payload);
    data.status = status;
    return data;
  }
);

export const publishRestaurantData: any = createAsyncThunk(
  "publishRestaurantData",
  async (payload) => {
    const { data, status } = await restaurantService.publishRestaurantData(
      payload
    );
    data.status = status;
    return data;
  }
);

const restaurantlice = createSlice({
  name: "restaurant",
  initialState,
  reducers: {},
  extraReducers: {
    ///--------------getRestaurant-------

    [getRestaurant.pending]: (state, action) => {
      state.loading = true;
    },
    [getRestaurant.fulfilled]: (state, { payload }) => {
      state.loading = false;
      state.restaurant = payload;
    },
    [getRestaurant.rejected]: (state, { payload }) => {
      state.loading = false;
    },

    ///--------------getRestaurantById-------

    [getRestaurantById.pending]: (state, action) => {
      state.loading = true;
    },
    [getRestaurantById.fulfilled]: (state, { payload }) => {
      state.loading = false;
      state.restaurant = payload;
    },
    [getRestaurantById.rejected]: (state, { payload }) => {
      state.loading = false;
    },

    ///deleteRestaurant

    [deleteRestaurant.pending]: (state, action) => {
      state.loading = true;
    },
    [deleteRestaurant.fulfilled]: (state, { payload }) => {
      state.loading = false;
      state.restaurant = payload;
    },
    [deleteRestaurant.rejected]: (state, { payload }) => {
      state.loading = false;
    },

    /// -------------uupdateRestaurant-----------
    [updateRestaurant.pending]: (state, action) => {
      state.loading = true;
    },
    [updateRestaurant.fulfilled]: (state, { payload }) => {
      state.loading = false;
      state.restaurant = payload;
    },
    [updateRestaurant.rejected]: (state, { payload }) => {
      state.loading = false;
    },

    /// -------------publish restaurant-----------
    [publishRestaurantData.pending]: (state, action) => {
      state.loading = true;
    },
    [publishRestaurantData.fulfilled]: (state, { payload }) => {
      state.loading = false;
      state.restaurant = payload;
    },
    [publishRestaurantData.rejected]: (state, { payload }) => {
      state.loading = false;
    },
  },
});

export default restaurantlice.reducer;
