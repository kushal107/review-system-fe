import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { customerService } from "../../../services/customerService";
const initialState = {
  customer: [],
  loading: false,
  error: "",
};

export const getCustomer: any = createAsyncThunk(
  "getCustomer",
  async (payload) => {
    const { data, status } = await customerService.getCustomer(payload);
    data.status = status;
    return data;
  }
);
export const getCustomerById: any = createAsyncThunk(
  "getCustomerById",
  async (payload) => {
    const { data, status } = await customerService.getCustomerById(payload);
    data.status = status;
    return data;
  }
);

export const addCustomer: any = createAsyncThunk(
  "updateCustomer",
  async (payload: any) => {
    const { data, status } = await customerService.addCustomer(
      payload.formData
    );
    data.status = status;
    return data;
  }
);

export const updateCustomer: any = createAsyncThunk(
  "updateCustomer",
  async (payload: any) => {
    const { data, status } = await customerService.updateCustomer(
      payload.formData,
      payload.id
    );
    data.status = status;
    return data;
  }
);

export const deleteCustomer: any = createAsyncThunk(
  "deleteCustomer",
  async (payload) => {
    const { data, status } = await customerService.deleteCustomer(payload);
    data.status = status;
    return data;
  }
);

const customerSlice = createSlice({
  name: "customer",
  initialState,
  reducers: {},
  extraReducers: {
    ///--------------getCustomer-------

    [getCustomer.pending]: (state, action) => {
      state.loading = true;
    },
    [getCustomer.fulfilled]: (state, { payload }) => {
      state.loading = false;
      state.customer = payload;
    },
    [getCustomer.rejected]: (state, { payload }) => {
      state.loading = false;
    },

    ///--------------getCustomerById-------

    [getCustomerById.pending]: (state, action) => {
      state.loading = true;
    },
    [getCustomerById.fulfilled]: (state, { payload }) => {
      state.loading = false;
      state.customer = payload;
    },
    [getCustomerById.rejected]: (state, { payload }) => {
      state.loading = false;
    },

    ///deleteCustomer

    [deleteCustomer.pending]: (state, action) => {
      state.loading = true;
    },
    [deleteCustomer.fulfilled]: (state, { payload }) => {
      state.loading = false;
      state.customer = payload;
    },
    [deleteCustomer.rejected]: (state, { payload }) => {
      state.loading = false;
    },

    /// -------------uupdateCustomer-----------
    [updateCustomer.pending]: (state, action) => {
      state.loading = true;
    },
    [updateCustomer.fulfilled]: (state, { payload }) => {
      state.loading = false;
      state.customer = payload;
    },
    [updateCustomer.rejected]: (state, { payload }) => {
      state.loading = false;
    },

    /// -------------addCustomer-----------
    [addCustomer.pending]: (state, action) => {
      state.loading = true;
    },
    [addCustomer.fulfilled]: (state, { payload }) => {
      state.loading = false;
      state.customer = payload;
    },
    [addCustomer.rejected]: (state, { payload }) => {
      state.loading = false;
    },
  },
});

export default customerSlice.reducer;
