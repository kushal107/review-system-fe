import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { reviewService } from "../../../services/reviewService";
const initialState = {
  review: [],
  loading: false,
  error: "",
};

export const getReview: any = createAsyncThunk("getReview", async (payload) => {
  const { data, status } = await reviewService.getReview(payload);
  data.status = status;
  return data;
});
export const getReviewById: any = createAsyncThunk(
  "getReviewById",
  async (payload) => {
    const { data, status } = await reviewService.getReviewById(payload);
    data.status = status;
    return data;
  }
);

export const deleteReview: any = createAsyncThunk(
  "deleteReview",
  async (payload) => {
    const { data, status } = await reviewService.deleteReview(payload);
    data.status = status;
    return data;
  }
);

const reviewSlice = createSlice({
  name: "review",
  initialState,
  reducers: {},
  extraReducers: {
    ///--------------getReview-------

    [getReview.pending]: (state, action) => {
      state.loading = true;
    },
    [getReview.fulfilled]: (state, { payload }) => {
      state.loading = false;
      state.review = payload;
    },
    [getReview.rejected]: (state, { payload }) => {
      state.loading = false;
    },

    ///--------------getReviewById-------

    [getReviewById.pending]: (state, action) => {
      state.loading = true;
    },
    [getReviewById.fulfilled]: (state, { payload }) => {
      state.loading = false;
      state.review = payload;
    },
    [getReviewById.rejected]: (state, { payload }) => {
      state.loading = false;
    },

    ///deleteReview

    [deleteReview.pending]: (state, action) => {
      state.loading = true;
    },
    [deleteReview.fulfilled]: (state, { payload }) => {
      state.loading = false;
      state.review = payload;
    },
    [deleteReview.rejected]: (state, { payload }) => {
      state.loading = false;
    },
  },
});

export default reviewSlice.reducer;
