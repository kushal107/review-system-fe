import { configureStore } from "@reduxjs/toolkit";
import AuthSlice from "./reducers/authSlice";

const store = configureStore({
  reducer: {
    user: AuthSlice,
  },
});

export type AppDispatch = typeof store.dispatch;
export default store;
