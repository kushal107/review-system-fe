import { useDispatch } from "react-redux";
import { Container } from "reactstrap";
import { Col, Row } from "reactstrap";
import { toast } from "react-toastify";
import { deleteMenuItem } from "@/redux/reducers/menuItemSlice";
import CommonModal from "../../../CommonElements/Ui-kits/CommonModal";
import { useState } from "react";
import Image from "next/image";
import Icon from "@mdi/react";
import { mdiChiliMedium, mdiCurrencyRupee, mdiSquareCircle } from "@mdi/js";

const MenuItemViewList = ({
  menuItemsArray,
  fetchMenuItems,
  setMenuItemsArray,
  menuCategoryId,
  setMenuItemDetails,
  toggleOutside,
}) => {
  const dispatch = useDispatch();

  const [modalForDelete, setModalForDelete] = useState<boolean>(false);
  const toggleForDelete = () => {
    setModalForDelete(!modalForDelete);
  };

  const handleEdit = (item) => {
    setMenuItemDetails({
      name: item.name ? item.name : "",
      description: item.description ? item.description : "",
      price: item.price ? item.price : null,
      isVeg: item.isVeg,
      isJain: item.isJain,
      isSpicy: item.isSpicy,
      isHalf: item.isHalf,
      calories: item.calories ? item.calories : null,
      categoryId: menuCategoryId,
      id: item.id,
    });

    toggleOutside();
  };

  const ModalDataForDelete = {
    isOpen: modalForDelete,
    header: false,
    footer: true,
    toggler: toggleForDelete,
    class: "modal-dialog-centered",
    title: "",
  };

  const [selectedMenuItem, setSelectedMenuItem] = useState(null);

  const deleteRev = async () => {
    const deleteRes = await dispatch(deleteMenuItem(selectedMenuItem.id));
    if (!deleteRes.payload) {
      return;
    }
    if (
      deleteRes.payload.status === 200 ||
      deleteRes.payload.status === "200"
    ) {
      toast.success("MenuItem deleted successfully");
      setMenuItemsArray([]);
      fetchMenuItems(true);
    }
  };

  const handleDel = (item) => {
    setSelectedMenuItem(item);
    toggleForDelete();
  };

  return (
    <Container fluid>
      <div className="list-group main-lists-content">
        <Row>
          {menuItemsArray &&
            menuItemsArray.map((item, index) => (
              <>
                <Col md={6}>
                  <a
                    className={`list-group-item list-group-item-action`}
                    href="#"
                    key={index}
                    style={{ margin: "5px 0px" }}
                  >
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                      }}
                    >
                      <div style={{ display: "flex", gap: "5px" }}>
                        {item.isVeg && (
                          <Icon
                            style={{ color: "green" }}
                            path={mdiSquareCircle}
                            size={1}
                          />
                        )}

                        {!item.isVeg && (
                          <Icon
                            style={{ color: "red" }}
                            path={mdiSquareCircle}
                            size={1}
                          />
                        )}

                        {item.isSpicy && (
                          <Icon
                            style={{ color: "red" }}
                            path={mdiChiliMedium}
                            size={1}
                          />
                        )}

                        {item.isJain && <h4>J</h4>}
                      </div>
                      <div style={{ display: "flex", gap: "10px" }}>
                        <i
                          onClick={() => handleEdit(item)}
                          className="icofont icofont-ui-edit"
                          style={{ fontSize: "20px", cursor: "pointer" }}
                        ></i>
                        <i
                          onClick={() => handleDel(item)}
                          className="txt-danger icofont icofont-ui-delete"
                          style={{ fontSize: "20px", cursor: "pointer" }}
                        />
                      </div>
                    </div>

                    <div className="d-flex w-100 justify-content-between align-items-center">
                      <div className="list-wrapper">
                        <span style={{ fontSize: "24px", fontWeight: "600" }}>
                          {item.name}
                        </span>
                      </div>
                    </div>
                    <p
                      style={{
                        fontSize: "13px",
                        color: "gray",
                        margin: "0",
                      }}
                    >
                      {item.description}
                    </p>

                    <p
                      style={{
                        display: "flex",
                        alignItems: "center",
                        fontSize: "15px",
                        margin: "0",
                      }}
                    >
                      Calories : {item.calories}{" "}
                      {/* <Image
                        width={100}
                        height={100}
                        src={`/assets/images/calorie.png`}
                        alt="successful"
                        style={{
                          width: "30px",
                          height: "30px",
                          position: "relative",
                          top: "-2px",
                        }}
                      /> */}
                    </p>

                    <p
                      style={{
                        display: "flex",
                        alignItems: "center",
                        fontSize: "15px",
                      }}
                    >
                      Quantity : {item.isHalf ? "Half" : "Full"}{" "}
                    </p>

                    <small
                      style={{
                        fontSize: "1.5em",
                        display: "flex",
                        justifyContent: "flex-end",
                        marginRight: "5px",
                        fontWeight: "bold",
                        position: "absolute",
                        bottom: "10px",
                        right: "15px",
                        alignItems: "center",
                      }}
                    >
                      {item.price}
                      <Icon path={mdiCurrencyRupee} size={1} />
                    </small>
                  </a>
                </Col>
              </>
            ))}
        </Row>
      </div>

      {modalForDelete && (
        <CommonModal
          modalData={ModalDataForDelete}
          clickFn={() => {
            toggleForDelete();
            deleteRev();
          }}
        >
          <div className="modal-toggle-wrapper">
            <ul className="modal-img">
              <li>
                <Image
                  src={"/assets/gif/danger.gif"}
                  alt="error"
                  width={100}
                  height={100}
                />
              </li>
            </ul>
            <h4 className="text-center pb-2">
              Are you sure you want to perform this Action ?
            </h4>
            <p className="text-center">
              If you allow this Action then it will delete the MenuItem
            </p>
          </div>
        </CommonModal>
      )}
    </Container>
  );
};

export default MenuItemViewList;
