import Image from "next/image";
import { useState } from "react";
import CommonModal from "../../../../CommonElements/Ui-kits/CommonModal";
import { useDispatch } from "react-redux";
import { deleteMenuCategory } from "@/redux/reducers/menuCategorySlice";
import { toast } from "react-toastify";
import Link from "next/link";

const MenuCategoryCardsFooter = ({
  item,
  fetchMenuCategory,
  setMenuCategoryArray,
  setCategoryDetails,
  setImage,
  setPreviewImage,
  toggle,
}) => {
  const dispatch = useDispatch();

  const [modalForDelete, setModalForDelete] = useState<boolean>(false);
  const toggleForDelete = () => {
    setModalForDelete(!modalForDelete);
  };

  const deleteRest = async () => {
    const deleteRes = await dispatch(deleteMenuCategory(item.id));
    if (!deleteRes.payload) {
      return;
    }
    if (
      deleteRes.payload.status === 200 ||
      deleteRes.payload.status === "200"
    ) {
      toast.success("MenuCategory deleted successfully");
      setMenuCategoryArray([]);
      fetchMenuCategory(true);
    }
  };

  const ModalDataForDelete = {
    isOpen: modalForDelete,
    header: false,
    footer: true,
    toggler: toggleForDelete,
    class: "modal-dialog-centered",
    title: "",
  };

  const handleEdit = (item) => {
    setCategoryDetails({
      catName: item.catName,
      thumbnail: item.thumbnail,
      id: item.id,
    });
    setImage(item.thumbnail);
    setPreviewImage(item.thumbnail);
    toggle();
  };

  return (
    <ul className="restaurant-action">
      <li onClick={() => toggleForDelete()}>
        <i className="icofont icofont-ui-delete"></i>
        <span className="f-light mt-2">Delete</span>
      </li>
      {modalForDelete && (
        <CommonModal
          modalData={ModalDataForDelete}
          clickFn={() => {
            toggleForDelete();
            deleteRest();
          }}
        >
          <div className="modal-toggle-wrapper">
            <ul className="modal-img">
              <li>
                <Image
                  src={"/assets/gif/danger.gif"}
                  alt="error"
                  width={100}
                  height={100}
                />
              </li>
            </ul>
            <h4 className="text-center pb-2">
              Are you sure you want to perform this Action ?
            </h4>
            <p className="text-center">
              If you allow this Action then it will delete the Category
            </p>
          </div>
        </CommonModal>
      )}
      <li onClick={() => handleEdit(item)}>
        <i className="icofont icofont-ui-edit"></i>
        <span className="f-light mt-2">Edit</span>
      </li>
      <li>
        <Link
          href={`/menuItems/${item.id}`}
          style={{ display: "flex", flexDirection: "column" }}
        >
          <i className="icofont icofont-restaurant-menu"></i>
          <span className="f-light mt-2">Items</span>
        </Link>
      </li>
    </ul>
  );
};

export default MenuCategoryCardsFooter;
