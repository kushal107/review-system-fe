import Image from "next/image";
import {
  Card,
  CardBody,
  Col,
  Container,
  FormGroup,
  Input,
  Row,
  Label,
} from "reactstrap";
import SvgIcon from "../../../../CommonElements/Icons/SvgIcon";
import { Href, filesFormats } from "../../../../utils/Constant";
import MenuCategoryCardsFooter from "./MenuCategoryCardsFooter";
import { useDispatch, useSelector } from "react-redux";
import {
  addMenuCategory,
  publishMenuCategoryData,
  updateMenuCategory,
} from "@/redux/reducers/menuCategorySlice";
import CommonModal from "../../../../CommonElements/Ui-kits/CommonModal";
import { toast } from "react-toastify";
import { PlusCircle } from "react-feather";
import { useRef, useState } from "react";

const MenuCardsContainer = ({
  menuCategoryArray,
  fetchMenuCategory,
  setMenuCategoryArray,
  restaurantId = null,
}) => {
  const [previewImage, setPreviewImage] = useState("");
  const [image, setImage] = useState("");
  const fileInputRef = useRef(null);
  const user = useSelector((state: any) => state.user?.user);

  const dispatch = useDispatch();

  const handleActiveInActive = async (item) => {
    const publishRes = await dispatch(publishMenuCategoryData(item.id));
    if (!publishRes.payload) {
      return;
    }
    if (
      publishRes.payload.status === 200 ||
      publishRes.payload.status === "200"
    ) {
      if (publishRes.payload.count === 1) {
        toast.success("MenuCategory is Active now");
      } else {
        toast.success("MenuCategory is InActive now");
      }
      setMenuCategoryArray([]);
      fetchMenuCategory(true);
    }
  };

  const [errors, setErrors]: any = useState({});

  const [categoryDetails, setCategoryDetails] = useState({
    catName: "",
    thumbnail: "",
    id: "",
  });

  const [modal, setModal] = useState<boolean>(false);
  const toggle = () => {
    setModal(!modal);
  };

  let ModalData = {
    isOpen: modal,
    header: true,
    footer: true,
    toggler: toggle,
    class: "modal-dialog-centered",
    title: categoryDetails.id ? "Edit Menu Category" : "Add Menu Category",
    button: categoryDetails.id ? "Update" : "Save",
    id: null,
  };

  const handleThumbnailImage = (e) => {
    const input = e.target;
    const imageObj = e.target.files;

    const upload_file = imageObj;
    const fileExtention = imageObj[0]?.name.split(".");
    const fsize = upload_file[0]?.size;
    if (!imageObj || !fileExtention || !fsize || !fileExtention.length) {
      return;
    }
    const file = Math.round(fsize / 1024);
    if (
      (upload_file && filesFormats.includes(upload_file.type)) ||
      filesFormats.includes("." + fileExtention[fileExtention.length - 1])
    ) {
      if (file >= 10000) {
        toast.error("File too Big, please select a file less than 10MB");
        input.value = "";
        if (!/safari/i.test(navigator.userAgent)) {
          input.type = "";
          input.type = "file";
        }

        setImage("");
        setPreviewImage("");
      }
      const { name } = e.target;
      if (e.target.files.length !== 0) {
        setImage(e.target.files[0]);
        setPreviewImage(URL.createObjectURL(e.target.files[0]));

        if (errors[name])
          setErrors((error) => {
            let errorNew = { ...error };
            delete errorNew[name];
            return errorNew;
          });
      }
    } else {
      toast.error("Only jpg, jpeg and png files are allowed!");
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setCategoryDetails({
      ...categoryDetails,
      [name]: value,
    });
    setErrors((error) => {
      let errorNew = { ...error };
      delete errorNew[name];
      return errorNew;
    });
  };

  const handleSubmitProfile = async (e?) => {
    let validateData = {
      catName: categoryDetails.catName,
      thumbnail: image,
    };

    let errors: any = {};

    if (!validateData.catName) {
      errors.catName = "Category Name is required";
    }

    if (!image) errors.thumbnail = "Image is required";

    const firstErrorField = Object.keys(errors)[0];
    if (firstErrorField) {
      const errorFieldElement = document.getElementById(firstErrorField)
        ? document.getElementById(firstErrorField)
        : document.getElementsByName(firstErrorField)[0];
      if (errorFieldElement) {
        errorFieldElement.scrollIntoView({
          behavior: "smooth",
          block: "center",
        });
      }
    }

    if (Object.keys(errors).length) {
      setErrors(errors);
      return;
    }

    const formData = new FormData();

    formData.append("catName", categoryDetails.catName);
    formData.append("userId", restaurantId ? restaurantId : user.id);

    if (image) formData.append("thumbnail", image);

    if (Object.keys(errors).length) {
      setErrors(errors);
      return;
    }

    if (categoryDetails.id) {
      const payload = {
        formData,
        id: categoryDetails.id,
      };

      const editCourseRes = await dispatch(updateMenuCategory(payload));
      if (!editCourseRes) {
        return;
      }
      if (
        editCourseRes.payload?.status === 200 ||
        editCourseRes.payload?.status === "200"
      ) {
        toggle();
        setMenuCategoryArray([]);
        fetchMenuCategory(true);
        toast.success("Review platform updated successfully");
        setCategoryDetails({
          catName: "",
          thumbnail: "",
          id: "",
        });

        setImage("");
        setPreviewImage("");
      }
    } else {
      const payload = {
        formData,
      };

      const editCourseRes = await dispatch(addMenuCategory(payload));
      if (!editCourseRes) {
        return;
      }
      if (
        editCourseRes.payload?.status === 200 ||
        editCourseRes.payload?.status === "200"
      ) {
        toggle();
        setMenuCategoryArray([]);
        fetchMenuCategory(true);
        toast.success("Review platform added successfully");
        setCategoryDetails({
          catName: "",
          thumbnail: "",
          id: "",
        });

        setImage("");
        setPreviewImage("");
      }
    }
  };

  return (
    <Container fluid>
      <Row className="mb-4">
        <Col md={6} style={{ display: "flex", alignItems: "center" }}></Col>
        <Col md={6} style={{ display: "flex", justifyContent: "end" }}>
          <span
            className="btn btn-primary"
            style={{
              color: "white",
              display: "flex",
              alignItems: "center",
            }}
            onClick={() => toggle()}
          >
            <PlusCircle
              style={{
                width: "20px",
                height: "20px",
                marginRight: "5px",
              }}
            />
            <span>Add Menu Category</span>
          </span>

          {modal && (
            <CommonModal
              modalData={ModalData}
              clickFn={() => {
                handleSubmitProfile();
              }}
            >
              <div className="modal-toggle-wrapper">
                <Row>
                  <Col
                    md={6}
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      margin: "15px 0px",
                      flexDirection: "column",
                      alignItems: "center",
                    }}
                  >
                    {previewImage ? (
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "center",
                          flexDirection: "column",
                          alignItems: "center",
                          gap: "10px",
                        }}
                      >
                        <Image
                          src={
                            previewImage
                              ? previewImage
                              : "/assets/img/placeholder.png"
                          }
                          alt="img not found"
                          width={500}
                          height={333}
                          style={{
                            maxWidth: "200px",
                            maxHeight: "200px",
                          }}
                          className="img-fluid rounded-3"
                        />
                        <i
                          onClick={() => fileInputRef.current.click()}
                          className="icofont icofont-ui-camera"
                          style={{ fontSize: "25px", cursor: "pointer" }}
                        ></i>
                      </div>
                    ) : (
                      <></>
                    )}

                    <div
                      className="custom-file-upload"
                      style={{ display: previewImage ? "none" : "" }}
                    >
                      <input
                        type="file"
                        id="myfile"
                        ref={fileInputRef}
                        name="thumbnail"
                        onChange={(e) => handleThumbnailImage(e)}
                        accept=".png, .jpg, .jpeg"
                        className="form-control"
                      />
                      <div
                        className="upload-div"
                        style={{ height: "200px", width: "200px" }}
                      >
                        <div>
                          <img
                            src="/assets/images/upload.svg"
                            height="24"
                            width="24"
                            alt="No image found"
                          />
                        </div>
                        <p>Upload Images of type jpg,jpeg or png upto 10 MB</p>
                      </div>
                    </div>

                    <p className="error-msg">{errors.thumbnail}</p>
                  </Col>
                  <Col md={6}>
                    <FormGroup>
                      <Label className="col-form-label">Category Name</Label>
                      <Input
                        type="text"
                        placeholder="Enter Category Name"
                        value={categoryDetails.catName}
                        name="catName"
                        onChange={(e) => handleChange(e)}
                      />
                      <p className="error-msg">{errors.catName}</p>
                    </FormGroup>
                  </Col>
                </Row>
              </div>
            </CommonModal>
          )}
        </Col>
      </Row>

      <Row className="user-cards-items">
        {menuCategoryArray.map((item, index) => (
          <Col xl={4} sm={6} xxl={3} className="col-ed-4 box-col-4" key={index}>
            <Card className="social-profile">
              <CardBody>
                <FormGroup
                  switch
                  check
                  style={{ display: "flex", justifyContent: "end" }}
                >
                  <Input
                    id="flexSwitchCheckChecked"
                    onChange={() => handleActiveInActive(item)}
                    type="switch"
                    checked={item.status == "Active" ? true : false}
                    style={{ fontSize: "20px", border: "1px solid" }}
                  />
                </FormGroup>

                <div className="social-img-wrap">
                  <div className="social-img">
                    <Image
                      width={68}
                      height={68}
                      src={
                        item.thumbnail
                          ? item.thumbnail
                          : "/assets/images/avatarForUser.png"
                      }
                      className="img-fluid"
                      alt="user"
                      style={{ height: "100px", width: "100%" }}
                    />
                  </div>
                  <div className="edit-icon">
                    <SvgIcon iconId="profile-check" />
                  </div>
                </div>
                <div className="social-details">
                  <h5 className="mb-1">
                    <a href={Href}>{item.catName}</a>
                  </h5>
                  {/* <span className="f-light">{item.email}</span> */}
                  {/* <SocialMediaIcons listClassName="card-social" /> */}
                  <MenuCategoryCardsFooter
                    item={item}
                    fetchMenuCategory={fetchMenuCategory}
                    setMenuCategoryArray={setMenuCategoryArray}
                    setCategoryDetails={setCategoryDetails}
                    setImage={setImage}
                    setPreviewImage={setPreviewImage}
                    toggle={toggle}
                  />
                </div>
              </CardBody>
            </Card>
          </Col>
        ))}
      </Row>
    </Container>
  );
};

export default MenuCardsContainer;
