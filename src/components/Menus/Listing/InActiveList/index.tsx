import MenuCardsContainer from "@/components/Menus/MenuCategoryCards";
import { getMenuCategory } from "@/redux/reducers/menuCategorySlice";
import { useEffect, useState } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import { useDispatch, useSelector } from "react-redux";

const ActiveList = ({ restaurantId = null }) => {
  const [menuCategoryArray, setMenuCategoryArray] = useState([]);
  const dispatch = useDispatch();
  const user = useSelector((state: any) => state.user?.user);
  // States for pagination
  const [totalData, setTotalData] = useState(0);
  const [currPage, setCurrPage] = useState(1);
  const [noOfRecords, setNoOfRecords] = useState(10);
  const [searchVal, setSearchVal] = useState("");
  const [sorting, setSorting] = useState({
    sortBy: "",
    order: "ASC",
  });
  const [hasMore, setHasMore] = useState(true);

  const fetchMenuCategory = async (fromAction?) => {
    let obj = {
      search: searchVal,
      pageRecord: noOfRecords,
      pageNo: fromAction ? 1 : currPage,
      sortBy: "",
      order: sorting.order,
      userId: restaurantId ? restaurantId : user?.id,
      isApproved: "InActive",
    };

    const res = await dispatch(getMenuCategory(obj));

    if (!res.payload) {
      return;
    }
    if (res.payload.status === 200 || res.payload.status === "200") {
      const newMenugetMenuCategoryList = res.payload.records;

      if (newMenugetMenuCategoryList.length === 0) {
        setHasMore(false); // No more data to fetch
        if (currPage == 1) {
          setMenuCategoryArray([]);
        }
      } else {
        setMenuCategoryArray((prevList) => [
          ...prevList,
          ...res.payload.records,
        ]);
        setTotalData(res.payload.totalCount);
      }
    }
  };

  useEffect(() => {
    fetchMenuCategory();
  }, [currPage, noOfRecords, searchVal, sorting]);

  return (
    <>
      {menuCategoryArray && menuCategoryArray.length === 0 ? (
        <>
          <div className="col-xxl-12 col-xl-12 col-lg-12">
            <div className="no-data-found">
              <img src="/assets/images/nodatafound.svg" alt="No Image found" />
              <h4>No Data Found!</h4>
            </div>
          </div>
        </>
      ) : (
        <>
          <InfiniteScroll
            className="infinite-scroll-component "
            dataLength={menuCategoryArray?.length}
            next={() => setCurrPage((prevPage) => prevPage + 1)}
            hasMore={hasMore}
            loader={null}
            endMessage={
              menuCategoryArray?.length > 0 && (
                <p style={{ textAlign: "center", marginTop: "1.5rem" }}>
                  <b>Yay! You have seen it all</b>
                </p>
              )
            }
          >
            <MenuCardsContainer
              menuCategoryArray={menuCategoryArray}
              fetchMenuCategory={fetchMenuCategory}
              setMenuCategoryArray={setMenuCategoryArray}
              restaurantId={restaurantId}
            />
          </InfiniteScroll>
        </>
      )}
    </>
  );
};

export default ActiveList;
