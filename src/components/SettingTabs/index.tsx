import {
  addReviewPlatform,
  deleteReviewPlatform,
  getReviewPlatform,
  updateReviewPlatform,
} from "@/redux/reducers/reviewPlatformSlice";
import Image from "next/image";
import React, { useEffect, useRef, useState } from "react";
import { PlusCircle } from "react-feather";
import { Item } from "react-photoswipe-gallery";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import {
  Card,
  CardBody,
  Col,
  FormGroup,
  Input,
  Label,
  Row,
  TabContent,
  TabPane,
} from "reactstrap";
import CardHead from "../../../CommonElements/CardHead";
import CommonModal from "../../../CommonElements/Ui-kits/CommonModal";
import { filesFormats } from "../../../utils/Constant";
import BorderNav from "./BorderNav";

const SettingTabs = () => {
  const [previewImage, setPreviewImage] = useState("");
  const [image, setImage] = useState("");
  const fileInputRef = useRef(null);
  const user = useSelector((state: any) => state.user?.user);
  console.log(user);
  const dispatch = useDispatch();

  const [basicTab, setBasicTab] = useState("reviewPlatform");

  const [modal, setModal] = useState<boolean>(false);
  const toggle = () => {
    setModal(!modal);
  };

  let ModalData = {
    isOpen: modal,
    header: false,
    footer: true,
    toggler: toggle,
    class: "modal-dialog-centered",
    title: "",
    button: "Save",
    id: null,
  };

  const [deleteModal, setDeleteModal] = useState<boolean>(false);
  const deleteToggle = () => {
    setDeleteModal(!deleteModal);
  };

  const deleteRest = async () => {
    const deleteRes = await dispatch(deleteReviewPlatform(platformDetails.id));
    if (!deleteRes.payload) {
      return;
    }
    if (
      deleteRes.payload.status === 200 ||
      deleteRes.payload.status === "200"
    ) {
      toast.success("Review Platform deleted successfully");
      fetchReviewPlatforms();
    }
  };

  const ModalDataForDelete = {
    isOpen: deleteModal,
    header: false,
    footer: true,
    toggler: deleteToggle,
    class: "modal-dialog-centered",
    title: "",
  };

  const [errors, setErrors]: any = useState({});

  const [platformDetails, setPlatformDetails] = useState({
    title: "",
    logo: "",
    id: "",
  });

  const [reviewPlatformList, setReviewPlatformList] = useState([]);

  const handleThumbnailImage = (e) => {
    const input = e.target;
    const imageObj = e.target.files;

    const upload_file = imageObj;
    const fileExtention = imageObj[0]?.name.split(".");
    const fsize = upload_file[0]?.size;
    if (!imageObj || !fileExtention || !fsize || !fileExtention.length) {
      return;
    }
    const file = Math.round(fsize / 1024);
    if (
      (upload_file && filesFormats.includes(upload_file.type)) ||
      filesFormats.includes("." + fileExtention[fileExtention.length - 1])
    ) {
      if (file >= 10000) {
        toast.error("File too Big, please select a file less than 10MB");
        input.value = "";
        if (!/safari/i.test(navigator.userAgent)) {
          input.type = "";
          input.type = "file";
        }

        setImage("");
        setPreviewImage("");
      }
      const { name } = e.target;
      if (e.target.files.length !== 0) {
        setImage(e.target.files[0]);
        setPreviewImage(URL.createObjectURL(e.target.files[0]));

        if (errors[name])
          setErrors((error) => {
            let errorNew = { ...error };
            delete errorNew[name];
            return errorNew;
          });
      }
    } else {
      toast.error("Only jpg, jpeg and png files are allowed!");
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setPlatformDetails({
      ...platformDetails,
      [name]: value,
    });
    setErrors((error) => {
      let errorNew = { ...error };
      delete errorNew[name];
      return errorNew;
    });
  };

  const handleSubmitProfile = async (e?) => {
    let validateData = {
      title: platformDetails.title,
      logo: image,
    };

    let errors: any = {};

    if (!validateData.title) {
      errors.title = "Title is required";
    }

    if (!image) errors.logo = "Image is required";

    const firstErrorField = Object.keys(errors)[0];
    if (firstErrorField) {
      const errorFieldElement = document.getElementById(firstErrorField)
        ? document.getElementById(firstErrorField)
        : document.getElementsByName(firstErrorField)[0];
      if (errorFieldElement) {
        errorFieldElement.scrollIntoView({
          behavior: "smooth",
          block: "center",
        });
      }
    }

    if (Object.keys(errors).length) {
      setErrors(errors);
      return;
    }

    const formData = new FormData();

    formData.append("title", platformDetails.title);

    if (image) formData.append("logo", image);

    if (Object.keys(errors).length) {
      setErrors(errors);
      return;
    }

    if (platformDetails.id) {
      const payload = {
        formData,
        id: platformDetails.id,
      };

      const editCourseRes = await dispatch(updateReviewPlatform(payload));
      if (!editCourseRes) {
        return;
      }
      if (
        editCourseRes.payload?.status === 200 ||
        editCourseRes.payload?.status === "200"
      ) {
        toggle();
        fetchReviewPlatforms();
        toast.success("Review platform updated successfully");
        setPlatformDetails({
          title: "",
          logo: "",
          id: "",
        });

        setImage("");
        setPreviewImage("");
      }
    } else {
      const payload = {
        formData,
      };

      const editCourseRes = await dispatch(addReviewPlatform(payload));
      if (!editCourseRes) {
        return;
      }
      if (
        editCourseRes.payload?.status === 200 ||
        editCourseRes.payload?.status === "200"
      ) {
        toggle();
        fetchReviewPlatforms();
        toast.success("Review platform added successfully");
        setPlatformDetails({
          title: "",
          logo: "",
          id: "",
        });

        setImage("");
        setPreviewImage("");
      }
    }
  };

  const fetchReviewPlatforms = async () => {
    const res = await dispatch(getReviewPlatform());
    if (!res.payload) {
      return;
    }
    if (res.payload.status === 200 || res.payload.status === "200") {
      setReviewPlatformList(res.payload?.records);
    }
  };

  const handleEdit = (item) => {
    setPlatformDetails({
      title: item.title,
      logo: item.logo,
      id: item.id,
    });
    setImage(item.logo);
    setPreviewImage(item.logo);
    toggle();
  };

  const handleDelete = (item) => {
    setPlatformDetails({
      title: item.title,
      logo: item.logo,
      id: item.id,
    });

    deleteToggle();
  };

  useEffect(() => {
    fetchReviewPlatforms();
  }, []);

  return (
    <Col xxl={12}>
      <Card>
        <CardHead title="Settings" />
        <CardBody>
          <BorderNav basicTab={basicTab} setBasicTab={setBasicTab} />
          <TabContent activeTab={basicTab}>
            <TabPane className="fade show" tabId="reviewPlatform">
              <div
                className="d-flex"
                style={{ justifyContent: "end", marginTop: "15px" }}
              >
                <span
                  className="btn btn-primary"
                  style={{
                    color: "white",
                    display: "flex",
                    alignItems: "center",
                  }}
                  onClick={() => toggle()}
                >
                  <PlusCircle
                    style={{
                      width: "20px",
                      height: "20px",
                      marginRight: "5px",
                    }}
                  />
                  <span>Add Platform</span>
                </span>

                {modal && (
                  <CommonModal
                    modalData={ModalData}
                    clickFn={() => {
                      handleSubmitProfile();
                    }}
                  >
                    <div className="modal-toggle-wrapper">
                      <Row>
                        <Col
                          md={6}
                          style={{
                            display: "flex",
                            justifyContent: "center",
                            margin: "15px 0px",
                            flexDirection: "column",
                            alignItems: "center",
                          }}
                        >
                          {previewImage ? (
                            <div
                              style={{
                                display: "flex",
                                justifyContent: "center",
                                flexDirection: "column",
                                alignItems: "center",
                                gap: "10px",
                              }}
                            >
                              <Image
                                src={
                                  previewImage
                                    ? previewImage
                                    : "/assets/img/placeholder.png"
                                }
                                alt="img not found"
                                width={500}
                                height={333}
                                style={{
                                  maxWidth: "200px",
                                  maxHeight: "200px",
                                }}
                                className="img-fluid rounded-3"
                              />
                              <i
                                onClick={() => fileInputRef.current.click()}
                                className="icofont icofont-ui-camera"
                                style={{ fontSize: "25px", cursor: "pointer" }}
                              ></i>
                            </div>
                          ) : (
                            <></>
                          )}

                          <div
                            className="custom-file-upload"
                            style={{ display: previewImage ? "none" : "" }}
                          >
                            <input
                              type="file"
                              id="myfile"
                              ref={fileInputRef}
                              name="logo"
                              onChange={(e) => handleThumbnailImage(e)}
                              accept=".png, .jpg, .jpeg"
                              className="form-control"
                            />
                            <div
                              className="upload-div"
                              style={{ height: "200px", width: "200px" }}
                            >
                              <div>
                                <img
                                  src="/assets/images/upload.svg"
                                  height="24"
                                  width="24"
                                  alt="No image found"
                                />
                              </div>
                              <p>
                                Upload Images of type jpg,jpeg or png upto 10 MB
                              </p>
                            </div>
                          </div>

                          <p className="error-msg">{errors.logo}</p>
                        </Col>
                        <Col md={6}>
                          <FormGroup>
                            <Label className="col-form-label">
                              Platform Title
                            </Label>
                            <Input
                              type="text"
                              placeholder="Enter Title"
                              value={platformDetails.title}
                              name="title"
                              onChange={(e) => handleChange(e)}
                            />
                            <p className="error-msg">{errors.title}</p>
                          </FormGroup>
                        </Col>
                      </Row>
                    </div>
                  </CommonModal>
                )}
              </div>
              <CardBody className="my-gallery row gallery-with-description">
                {reviewPlatformList && reviewPlatformList.length ? (
                  <Row>
                    {reviewPlatformList?.map((item, index) => (
                      <figure
                        key={index}
                        className="col-xl-3 col-sm-6 box-col-25"
                      >
                        <Item
                          original={item.logo}
                          width="1024"
                          height="768"
                          caption="images"
                        >
                          {({ ref, open }) => (
                            <div
                              className="gallery-detail"
                              style={{ position: "relative" }}
                            >
                              <div className="action-btn-image">
                                <i
                                  onClick={() => handleEdit(item)}
                                  style={{ cursor: "pointer" }}
                                  className="icofont icofont-ui-edit"
                                ></i>
                                <i
                                  onClick={() => handleDelete(item)}
                                  style={{ cursor: "pointer" }}
                                  className="icofont icofont-ui-delete"
                                ></i>
                              </div>

                              <Image
                                height={500}
                                width={500}
                                className="img-logo"
                                ref={
                                  ref as React.MutableRefObject<HTMLImageElement>
                                }
                                src={item.logo}
                                alt="logo"
                                style={{ height: "180px" }}
                              />
                              <div className="caption">
                                <h4>{item.title}</h4>
                                {/* <p>{Imagedescription}</p> */}
                              </div>
                            </div>
                          )}
                        </Item>
                      </figure>
                    ))}
                  </Row>
                ) : (
                  <></>
                )}

                {deleteModal && (
                  <CommonModal
                    modalData={ModalDataForDelete}
                    clickFn={() => {
                      deleteToggle();
                      deleteRest();
                    }}
                  >
                    <div className="modal-toggle-wrapper">
                      <ul className="modal-img">
                        <li>
                          <Image
                            src={"/assets/gif/danger.gif"}
                            alt="error"
                            width={100}
                            height={100}
                          />
                        </li>
                      </ul>
                      <h4 className="text-center pb-2">
                        Are you sure you want to perform this Action ?
                      </h4>
                      <p className="text-center">
                        If you allow this Action then it will delete the
                        platform
                      </p>
                    </div>
                  </CommonModal>
                )}
              </CardBody>
            </TabPane>

            {/* <TabPane className="fade show" tabId="2">
              <h3>BYEEE</h3>
            </TabPane> */}
          </TabContent>
        </CardBody>
      </Card>
    </Col>
  );
};

export default SettingTabs;
