import React from "react";
import { Nav, NavItem, NavLink } from "reactstrap";
import { Contact, Home, Inbox } from "../../../../utils/Constant";

type propsType = {
  basicTab: string;
  setBasicTab: (data: string) => void;
};

const BorderNav = ({ basicTab, setBasicTab }: propsType) => {
  return (
    <Nav tabs className="nav-tabs border-tab mb-0 nav-info">
      <NavItem>
        <NavLink
          href="#"
          className={`nav-border txt-info tab-info pt-0 ${
            basicTab === "reviewPlatform" ? "active" : ""
          }`}
          onClick={() => setBasicTab("reviewPlatform")}
        >
          <i className="icofont icofont-match-review"></i>Review Platforms
        </NavLink>
      </NavItem>
      {/* <NavItem>
        <NavLink
          href="#"
          className={`nav-border txt-info tab-info ${
            basicTab === "2" ? "active" : ""
          }`}
          onClick={() => setBasicTab("2")}
        >
          <i className="icofont icofont-ui-message" />
          {Inbox}
        </NavLink>
      </NavItem> */}
    </Nav>
  );
};

export default BorderNav;
