
const CustomerCardsFooter = ({ item }) => {
  return (
    <ul className="staff-action">
      <li>
        <i className="icofont icofont-ui-edit"></i>
        <span className="f-light mt-2">Edit</span>
      </li>
      <li>
        <i className="icofont icofont-ui-delete"></i>
        <span className="f-light mt-2">Delete</span>
      </li>
    </ul>
  );
};

export default CustomerCardsFooter;
