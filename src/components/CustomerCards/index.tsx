import Image from "next/image";
import { Card, CardBody, Col, Container, Row } from "reactstrap";
import { userCardData } from "../../../Data/Users";
import { Href, ImgPath } from "../../../utils/Constant";
import CustomerCardsFooter from "./CustomerCardsFooter";

const CustomerCardsContainer = () => {
  return (
    <Container fluid>
      <Row className="user-cards-items">
        {userCardData.map((item) => (
          <Col
            xl={4}
            sm={6}
            xxl={3}
            className="col-ed-4 box-col-4"
            key={item.id}
          >
            <Card className="social-profile">
              <CardBody>
                <div className="social-img-wrap">
                  <div className="social-img">
                    <Image
                      width={68}
                      height={68}
                      src="/assets/images/avatarForUser.png"
                      className="img-fluid"
                      alt="user"
                    />
                  </div>
                </div>
                <div className="social-details">
                  <h5 className="mb-1">
                    <a href={Href}>{item.name}</a>
                  </h5>
                  <span className="f-light">{item.userProfile}</span>
                  <CustomerCardsFooter item={item} />
                </div>
              </CardBody>
            </Card>
          </Col>
        ))}
      </Row>
    </Container>
  );
};

export default CustomerCardsContainer;
