import { useDispatch } from "react-redux";
import { Container } from "reactstrap";
import StarRatings from "react-star-ratings";
import { toast } from "react-toastify";
import { deleteReview } from "@/redux/reducers/reviewSlice";
import CommonModal from "../../../CommonElements/Ui-kits/CommonModal";
import { useState } from "react";
import Image from "next/image";

const ReviewViewList = ({ reviewsArray, fetchReviews, setReviewsArray }) => {
  const dispatch = useDispatch();

  const [modal, setModal] = useState<boolean>(false);
  const toggle = () => {
    setModal(!modal);
  };

  const ModalData = {
    isOpen: modal,
    header: false,
    footer: true,
    toggler: toggle,
    class: "modal-dialog-centered",
    title: "",
  };

  const [selectedReview, setSelectedReview] = useState(null);

  const deleteRev = async () => {
    const deleteRes = await dispatch(deleteReview(selectedReview.id));
    if (!deleteRes.payload) {
      return;
    }
    if (
      deleteRes.payload.status === 200 ||
      deleteRes.payload.status === "200"
    ) {
      toast.success("Review deleted successfully");
      setReviewsArray([]);
      fetchReviews(true);
    }
  };

  const handleDel = (item) => {
    setSelectedReview(item);
    toggle();
  };

  const formatTimeDifference = (timestamp) => {
    const currentTime: any = new Date();
    const targetTime: any = new Date(timestamp);
    const timeDifference = currentTime - targetTime;

    const minute = 60 * 1000;
    const hour = 60 * minute;
    const day = 24 * hour;
    const month = 30 * day;
    const year = 365 * day;

    if (timeDifference < 0) {
      return "In the future";
    } else if (timeDifference < 2 * minute) {
      return "Just now";
    } else if (timeDifference < hour) {
      const minutes = Math.floor(timeDifference / minute);
      return `${minutes} minute${minutes > 1 ? "s" : ""} ago`;
    } else if (timeDifference < 2 * hour) {
      return "An hour ago";
    } else if (timeDifference < day) {
      const hours = Math.floor(timeDifference / hour);
      return `${hours} hour${hours > 1 ? "s" : ""} ago`;
    } else if (timeDifference < 2 * day) {
      return "Yesterday";
    } else if (timeDifference < month) {
      const days = Math.floor(timeDifference / day);
      return `${days} day${days > 1 ? "s" : ""} ago`;
    } else if (timeDifference < 2 * month) {
      return "Last month";
    } else if (timeDifference < year) {
      const months = Math.floor(timeDifference / month);
      return `${months} month${months > 1 ? "s" : ""} ago`;
    } else {
      const years = Math.floor(timeDifference / year);
      return `${years} year${years > 1 ? "s" : ""} ago`;
    }
  };

  reviewsArray.forEach((element) => {
    element["issues"] = element.issueType.join(", ");
    element["period"] = formatTimeDifference(element.createdAt);
  });

  return (
    <Container fluid>
      <div className="list-group main-lists-content">
        {reviewsArray &&
          reviewsArray.map((item, index) => (
            <>
              <a
                className={`list-group-item list-group-item-action`}
                href="#"
                key={index}
                style={{ margin: "5px 0px" }}
              >
                <div className="d-flex w-100 justify-content-between align-items-center">
                  <div className="list-wrapper">
                    <StarRatings
                      rating={item.rating}
                      starRatedColor="#7366FF"
                      numberOfStars={5}
                      name="rating"
                    />
                  </div>

                  <i
                    onClick={() => handleDel(item)}
                    className="txt-danger icofont icofont-ui-delete"
                    style={{ fontSize: "20px", cursor: "pointer" }}
                  />
                </div>
                <div
                  style={{
                    marginTop: "15px",
                    display: "flex",
                    flexDirection: "column",
                    gap: "2px",
                  }}
                >
                  <h6>Issues</h6>
                  <p>{item.issues}</p>
                </div>

                <div
                  style={{
                    marginTop: "5px",
                    display: "flex",
                    flexDirection: "column",
                    gap: "2px",
                  }}
                >
                  <h6>Review</h6>
                  <p className="mb-1">{item.description}</p>
                </div>
                <small
                  style={{
                    fontSize: "0.875em",
                    display: "flex",
                    justifyContent: "flex-end",
                    marginRight: "5px",
                  }}
                >
                  {item.period}
                </small>
              </a>
            </>
          ))}
      </div>

      {modal && (
        <CommonModal
          modalData={ModalData}
          clickFn={() => {
            toggle();
            deleteRev();
          }}
        >
          <div className="modal-toggle-wrapper">
            <ul className="modal-img">
              <li>
                <Image
                  src={"/assets/gif/danger.gif"}
                  alt="error"
                  width={100}
                  height={100}
                />
              </li>
            </ul>
            <h4 className="text-center pb-2">
              Are you sure you want to perform this Action ?
            </h4>
            <p className="text-center">
              If you allow this Action then it will delete the Review
            </p>
          </div>
        </CommonModal>
      )}
    </Container>
  );
};

export default ReviewViewList;
