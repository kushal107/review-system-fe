import Image from "next/image";
import {
  Card,
  CardBody,
  Col,
  Container,
  FormGroup,
  Input,
  Row,
} from "reactstrap";
import SvgIcon from "../../../../CommonElements/Icons/SvgIcon";
import { Href } from "../../../../utils/Constant";
import RestaurantCardsFooter from "./RestaurantCardsFooter";
import { useDispatch } from "react-redux";
import { publishRestaurantData } from "@/redux/reducers/restaurantSlice";
import { toast } from "react-toastify";

const RestaurantCardsContainer = ({
  restaurantsArray,
  fetchRestaurants,
  setRestaurantsArray,
}) => {
  const dispatch = useDispatch();

  const handleActiveInActive = async (item) => {
    const publishRes = await dispatch(publishRestaurantData(item.id));
    if (!publishRes.payload) {
      return;
    }
    if (
      publishRes.payload.status === 200 ||
      publishRes.payload.status === "200"
    ) {
      if (publishRes.payload.count === 1) {
        toast.success("Restaurant is Active now");
      } else {
        toast.success("Restaurant is InActive now");
      }
      setRestaurantsArray([]);
      fetchRestaurants(true);
    }
  };

  return (
    <Container fluid>
      <Row className="user-cards-items">
        {restaurantsArray.map((item, index) => (
          <Col xl={4} sm={6} xxl={3} className="col-ed-4 box-col-4" key={index}>
            <Card className="social-profile">
              <CardBody>
                <FormGroup
                  switch
                  check
                  style={{ display: "flex", justifyContent: "end" }}
                >
                  <Input
                    id="flexSwitchCheckChecked"
                    onChange={() => handleActiveInActive(item)}
                    type="switch"
                    checked={item.isApproved == "Active" ? true : false}
                    style={{ fontSize: "20px", border: "1px solid" }}
                  />
                </FormGroup>

                <div className="social-img-wrap">
                  <div className="social-img">
                    <Image
                      width={68}
                      height={68}
                      src={
                        item.RestaurantProfile
                          ? item.RestaurantProfile?.logo
                          ? item.RestaurantProfile?.logo
                          : "/assets/images/avatarForUser.png"
                          : "/assets/images/avatarForUser.png"
                      }
                      className="img-fluid"
                      alt="user"
                    />
                  </div>
                  <div className="edit-icon">
                    <SvgIcon iconId="profile-check" />
                  </div>
                </div>
                <div className="social-details">
                  <h5 className="mb-1">
                    <a href={Href}>{item.fullName}</a>
                  </h5>
                  <span className="f-light">{item.email}</span>
                  {/* <SocialMediaIcons listClassName="card-social" /> */}
                  <RestaurantCardsFooter
                    item={item}
                    fetchRestaurants={fetchRestaurants}
                    setRestaurantsArray={setRestaurantsArray}
                  />
                </div>
              </CardBody>
            </Card>
          </Col>
        ))}
      </Row>
    </Container>
  );
};

export default RestaurantCardsContainer;
