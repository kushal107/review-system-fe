import Image from "next/image";
import { useState } from "react";
import CommonModal from "../../../../CommonElements/Ui-kits/CommonModal";
import { useDispatch } from "react-redux";
import { deleteRestaurant } from "@/redux/reducers/restaurantSlice";
import { toast } from "react-toastify";
import Link from "next/link";

const RestaurantCardsFooter = ({
  item,
  fetchRestaurants,
  setRestaurantsArray,
}) => {
  const dispatch = useDispatch();

  const [modal, setModal] = useState<boolean>(false);
  const toggle = () => {
    setModal(!modal);
  };

  const deleteRest = async () => {
    const deleteRes = await dispatch(deleteRestaurant(item.id));
    if (!deleteRes.payload) {
      return;
    }
    if (
      deleteRes.payload.status === 200 ||
      deleteRes.payload.status === "200"
    ) {
      toast.success("Restaurant deleted successfully");
      setRestaurantsArray([]);
      fetchRestaurants(true);
    }
  };

  const ModalData = {
    isOpen: modal,
    header: false,
    footer: true,
    toggler: toggle,
    class: "modal-dialog-centered",
    title: "",
  };

  return (
    <ul className="restaurant-action">
      <li onClick={() => toggle()}>
        <i className="icofont icofont-ui-delete"></i>
        <span className="f-light mt-2">Delete</span>
      </li>
      {modal && (
        <CommonModal
          modalData={ModalData}
          clickFn={() => {
            toggle();
            deleteRest();
          }}
        >
          <div className="modal-toggle-wrapper">
            <ul className="modal-img">
              <li>
                <Image
                  src={"/assets/gif/danger.gif"}
                  alt="error"
                  width={100}
                  height={100}
                />
              </li>
            </ul>
            <h4 className="text-center pb-2">
              Are you sure you want to perform this Action ?
            </h4>
            <p className="text-center">
              {item.isApproved == "Active"
                ? "If you allow this Action then it will make the Restaurant as InActive"
                : "If you allow this Action then it will make the Restaurant as Active"}
            </p>
          </div>
        </CommonModal>
      )}
      <li>
        <Link href={`/reviews/${item.id}`}>
          <img
            src="/assets/images/dashboard-5/social/review-like-message-svgrepo-com.png"
            alt="No image found"
            style={{ width: "35px", height: "33px" }}
          />
          <span className="f-light">Reviews</span>
        </Link>
      </li>
      <li>
        <Link
          href={`/menus/${item.id}`}
          style={{ display: "flex", flexDirection: "column" }}
        >
          <i className="icofont icofont-restaurant-menu"></i>
          <span className="f-light mt-2">Menu</span>
        </Link>
      </li>
    </ul>
  );
};

export default RestaurantCardsFooter;
