import {
  Button,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  FormGroup,
  Input,
  Label,
  Row,
} from "reactstrap";
import {
  Address,
  City,
  Country,
  EmailAddress,
  PostalCode,
  State,
  UpdateProfile,
  filesFormats,
} from "../../../../utils/Constant";

import { fetchUser } from "@/redux/reducers/authSlice";
import { updateRestaurant } from "@/redux/reducers/restaurantSlice";
import axios from "axios";
import Image from "next/image";
import { useRouter } from "next/router";
import { useEffect, useRef, useState } from "react";
import QRCode from "react-qr-code";
import html2canvas from "html2canvas";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import { validateEditProfile } from "../../../../utils/validation";
import { getReviewPlatform } from "@/redux/reducers/reviewPlatformSlice";

const AddEditRestaurant = ({}) => {
  const [previewImage, setPreviewImage] = useState("");
  const [image, setImage] = useState("");
  const fileInputRef = useRef(null);
  const user = useSelector((state: any) => state.user?.user);
  console.log(user);
  const dispatch = useDispatch();
  const router = useRouter();

  const [errors, setErrors]: any = useState({});

  const [restaurantDetails, setRestaurantDetails] = useState({
    fullName: "",
    address: "",
    pinCode: "",
    state: "",
    city: "",
    country: "",
    email: "",
    phoneNo: "",
    logo: "",
    qrCode: "",
    reviewPlatformLinks: [
      {
        platform: "",
        link: "",
        isSelected: null,
      },
    ],
  });

  const fetchRestaurantData = async () => {
    const signInUserResponse = await dispatch(fetchUser());
    if (!signInUserResponse.payload) {
      return;
    }
    if (
      signInUserResponse.payload.status === 200 ||
      signInUserResponse.payload.status === "200"
    ) {
      const data = signInUserResponse.payload;
      setRestaurantDetails({
        fullName: data.fullName ? data.fullName : "",
        address: data.RestaurantProfile
          ? data?.RestaurantProfile.address
            ? data?.RestaurantProfile.address
            : ""
          : "",
        pinCode: data.RestaurantProfile
          ? data?.RestaurantProfile.pinCode
            ? data?.RestaurantProfile.pinCode
            : ""
          : "",
        state: data.RestaurantProfile
          ? data?.RestaurantProfile.state
            ? data?.RestaurantProfile.state
            : ""
          : "",
        city: data.RestaurantProfile
          ? data?.RestaurantProfile.city
            ? data?.RestaurantProfile.city
            : ""
          : "",
        country: data.RestaurantProfile
          ? data?.RestaurantProfile.country
            ? data?.RestaurantProfile.country
            : ""
          : "",
        email: data.email ? data.email : "",
        phoneNo: data.RestaurantProfile
          ? data?.RestaurantProfile.phoneNo
            ? data?.RestaurantProfile.phoneNo
            : ""
          : "",
        logo: data.RestaurantProfile
          ? data?.RestaurantProfile.logo
            ? data?.RestaurantProfile.logo
            : ""
          : "",
        qrCode: data.RestaurantProfile
          ? data?.RestaurantProfile.qrCode
            ? data?.RestaurantProfile.qrCode
            : data.fullName
            ? convertName(data.fullName)
            : ""
          : data.fullName
          ? convertName(data.fullName)
          : "",
        reviewPlatformLinks: data.RestaurantProfile
          ? data?.RestaurantProfile.SocialLinks &&
            data?.RestaurantProfile.SocialLinks.length
            ? data?.RestaurantProfile.SocialLinks.map((v) => ({
                platform: v.ReviewPlatform?.id,
                link: v.socialLinks,
                isSelected: v.isSelected,
              }))
            : [
                {
                  platform: "",
                  link: "",
                  isSelected: null,
                },
              ]
          : [
              {
                platform: "",
                link: "",
                isSelected: null,
              },
            ],
      });

      setImage(
        data.RestaurantProfile
          ? data?.RestaurantProfile.logo
            ? data?.RestaurantProfile.logo
            : ""
          : ""
      );

      setPreviewImage(
        data.RestaurantProfile
          ? data?.RestaurantProfile.logo
            ? data?.RestaurantProfile.logo
            : ""
          : ""
      );
    }
  };

  const generateRandomChars = (length) => {
    const characters = "abcdefghijklmnopqrstuvwxyz0123456789";
    let result = "";
    for (let i = 0; i < length; i++) {
      const randomIndex = Math.floor(Math.random() * characters.length);
      result += characters.charAt(randomIndex);
    }
    return result;
  };

  const convertName = (input) => {
    const words = input.split(" ");
    console.log("first", words);
    if (words.length === 1) {
      return words[0].slice(0, 3).toLowerCase() + "-" + generateRandomChars(3);
    } else if (words.length === 2) {
      const firstPart = words[0].slice(0, 3).toLowerCase();
      const secondPart = words[1][0].toLowerCase();
      return firstPart + secondPart + "-" + generateRandomChars(3);
    } else {
      const initials = words.map((word) =>
        word.length > 0 ? word[0].toLowerCase() : ""
      );

      return initials.join("") + "-" + generateRandomChars(3);
    }
  };

  useEffect(() => {
    if (user?.id) {
      fetchRestaurantData();
    }
    fetchReviewPlatforms();
  }, [user.id]);

  const handleThumbnailImage = (e) => {
    const input = e.target;
    const imageObj = e.target.files;

    const upload_file = imageObj;
    const fileExtention = imageObj[0]?.name.split(".");
    const fsize = upload_file[0]?.size;
    if (!imageObj || !fileExtention || !fsize || !fileExtention.length) {
      return;
    }
    const file = Math.round(fsize / 1024);
    if (
      (upload_file && filesFormats.includes(upload_file.type)) ||
      filesFormats.includes("." + fileExtention[fileExtention.length - 1])
    ) {
      if (file >= 10000) {
        toast.error("File too Big, please select a file less than 10MB");
        input.value = "";
        if (!/safari/i.test(navigator.userAgent)) {
          input.type = "";
          input.type = "file";
        }

        setImage("");
        setPreviewImage("");
      }
      const { name } = e.target;
      if (e.target.files.length !== 0) {
        setImage(e.target.files[0]);
        setPreviewImage(URL.createObjectURL(e.target.files[0]));

        if (errors[name])
          setErrors((error) => {
            let errorNew = { ...error };
            delete errorNew[name];
            return errorNew;
          });
      }
    } else {
      toast.error("Only jpg, jpeg and png files are allowed!");
    }
  };

  const hamdleZipChange = async (e) => {
    const { name, value } = e.target;

    setRestaurantDetails({
      ...restaurantDetails,
      ["country"]: "",
      ["state"]: "",
      ["city"]: "",
      ["pinCode"]: value,
    });

    if (value && value.length > 4) {
      setRestaurantDetails({
        ...restaurantDetails,
        ["pinCode"]: value,
      });
      const url = `https://maps.googleapis.com/maps/api/geocode/json?address=${value}&key=${process.env.NEXT_PUBLIC_GOOGLE_API_KEY}`;

      const response = await axios.get(url);

      const result = response.data.results[0];
      const countryObj = result?.address_components.find((obj) =>
        obj.types.includes("country")
      );
      const country = countryObj ? countryObj.long_name : "";

      const stateObj = result?.address_components?.find((obj) =>
        obj.types.includes("administrative_area_level_1")
      );
      const state = stateObj ? stateObj.long_name : "";

      const cityObj = result?.address_components?.find(
        (obj) =>
          obj.types.includes("locality") || obj.types.includes("neighborhood")
      );
      const city = cityObj ? cityObj.long_name : "";

      setRestaurantDetails({
        ...restaurantDetails,
        ["city"]: city,
        ["country"]: country,
        ["state"]: state,
        ["pinCode"]: value,
      });

      if (errors["pinCode"])
        setErrors((error) => {
          let errorNew = { ...error };
          delete errorNew["pinCode"];
          return errorNew;
        });
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setRestaurantDetails({
      ...restaurantDetails,
      [name]: value,
    });
    setErrors((error) => {
      let errorNew = { ...error };
      delete errorNew[name];
      return errorNew;
    });
  };

  const handlePlatformChange = (e, item, index) => {
    const { name, value } = e.target;

    let list = [...restaurantDetails.reviewPlatformLinks];

    if (name == "isSelected") {
      list[index][name] = e.target.checked;
    } else {
      list[index][name] = value;
    }

    setRestaurantDetails({
      ...restaurantDetails,
      ["reviewPlatformLinks"]: list,
    });
  };

  const [reviewPlatformList, setReviewPlatformList] = useState([]);

  const fetchReviewPlatforms = async () => {
    const res = await dispatch(getReviewPlatform());
    if (!res.payload) {
      return;
    }
    if (res.payload.status === 200 || res.payload.status === "200") {
      setReviewPlatformList(res.payload?.records);
    }
  };

  const handleSubmitProfile = async (e?) => {
    if (e) {
      e.preventDefault();
    }
    let validateData = {
      fullName: restaurantDetails.fullName,
      address: restaurantDetails.address,
      pinCode: restaurantDetails.pinCode,
      state: restaurantDetails.state,
      city: restaurantDetails.city,
      country: restaurantDetails.state,
      email: restaurantDetails.email,
      phoneNo: restaurantDetails.phoneNo,
      logo: image,
      qrCode: restaurantDetails.qrCode,
    };

    const errors = validateEditProfile(validateData);

    if (!image) errors.logo = "Image is required";

    const firstErrorField = Object.keys(errors)[0];
    if (firstErrorField) {
      const errorFieldElement = document.getElementById(firstErrorField)
        ? document.getElementById(firstErrorField)
        : document.getElementsByName(firstErrorField)[0];
      if (errorFieldElement) {
        errorFieldElement.scrollIntoView({
          behavior: "smooth",
          block: "center",
        });
      }
    }

    if (Object.keys(errors).length) {
      setErrors(errors);
      return;
    }

    const formData = new FormData();

    formData.append("fullName", restaurantDetails.fullName);
    formData.append("address", restaurantDetails.address);
    formData.append("pinCode", restaurantDetails.pinCode);
    formData.append("state", restaurantDetails.state);
    formData.append("city", restaurantDetails.city);
    formData.append("country", restaurantDetails.country);
    formData.append("email", restaurantDetails.email);
    formData.append("phoneNo", restaurantDetails.phoneNo);
    formData.append("qrCode", restaurantDetails.qrCode);

    if (restaurantDetails.reviewPlatformLinks.length) {
      let valid = true;
      let isAnySelected = false;
      for (let ele of restaurantDetails.reviewPlatformLinks) {
        if (ele.link && ele.platform) {
        } else {
          valid = false;
          break;
        }
      }

      if (valid) {
      } else {
        toast.error("Please enter all details of Review Platforms");
        return;
      }

      for (let ele of restaurantDetails.reviewPlatformLinks) {
        if (ele.isSelected) {
          isAnySelected = true;
          break;
        }
      }

      if (isAnySelected) {
        formData.append(
          "reviewPlatforms",
          JSON.stringify(restaurantDetails.reviewPlatformLinks)
        );
      } else {
        toast.error("Any one platform should be selected");
        return;
      }
    }

    if (image) formData.append("logo", image);

    if (Object.keys(errors).length) {
      setErrors(errors);
      return;
    }
    const payload = {
      formData,
      id: user?.id,
    };

    const editCourseRes = await dispatch(updateRestaurant(payload));
    if (!editCourseRes) {
      return;
    }
    if (
      editCourseRes.payload?.status === 200 ||
      editCourseRes.payload?.status === "200"
    ) {
      // fetchCourses();
      toast.success("Profile updated successfully");
      setRestaurantDetails({
        fullName: "",
        address: "",
        pinCode: "",
        state: "",
        city: "",
        country: "",
        email: "",
        phoneNo: "",
        logo: "",
        qrCode: "",
        reviewPlatformLinks: [
          {
            platform: "",
            link: "",
            isSelected: null,
          },
        ],
      });

      setImage("");
      fetchRestaurantData();
    }
  };

  const addPlatforms = () => {
    let obj = {
      platform: "",
      link: "",
      isSelected: null,
    };

    const updatedRestaurantDetails = {
      ...restaurantDetails,
      reviewPlatformLinks: [...restaurantDetails.reviewPlatformLinks, obj],
    };
    setRestaurantDetails(updatedRestaurantDetails);
  };

  const deletePlatforms = (index) => {
    restaurantDetails.reviewPlatformLinks.splice(index, 1);

    const updatedRestaurantDetails = {
      ...restaurantDetails,
      reviewPlatformLinks: restaurantDetails.reviewPlatformLinks,
    };
    setRestaurantDetails(updatedRestaurantDetails);

    if (restaurantDetails.reviewPlatformLinks.length == 0) {
      addPlatforms();
    }
  };

  const downloadQRCode = () => {
    const qrCodeDiv = document.getElementById("qr-code"); // Get the DOM element containing the QR code

    setTimeout(() => {
      html2canvas(qrCodeDiv).then(function (canvas) {
        const link = document.createElement("a");
        link.href = canvas.toDataURL("image/png");
        link.download = "qrcode.png";
        link.click();
      });
    }, 100); // Adjust the delay as needed
  };

  return (
    <Col xl={12}>
      <form className="card" onSubmit={handleSubmitProfile}>
        <CardHeader>
          <h4 className="card-title mb-0">Edit Restaurant</h4>
        </CardHeader>
        <CardBody>
          <Row>
            <Col
              md={6}
              style={{
                display: "flex",
                justifyContent: "center",
                margin: "15px 0px",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <div
                id="qr-code"
                style={{
                  display: "flex",
                  justifyContent: "center",
                  flexDirection: "column",
                  alignItems: "center",
                  padding: "15px",
                  gap: "10px",
                }}
              >
                <h3>{restaurantDetails.qrCode}</h3>
                <QRCode
                  value={
                    restaurantDetails.qrCode ? restaurantDetails.qrCode : ""
                  }
                  style={{ height: "200px" }}
                />
              </div>

              <i
                onClick={downloadQRCode}
                className="icofont icofont-download-alt"
                style={{ cursor: "pointer", fontSize: "25px" }}
              ></i>
            </Col>
            <Col
              md={6}
              style={{
                display: "flex",
                justifyContent: "center",
                margin: "15px 0px",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              {previewImage ? (
                <div
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    flexDirection: "column",
                    alignItems: "center",
                    gap: "10px",
                  }}
                >
                  <Image
                    src={
                      previewImage
                        ? previewImage
                        : "/assets/img/placeholder.png"
                    }
                    alt="img not found"
                    width={500}
                    height={333}
                    style={{
                      maxWidth: "200px",
                      maxHeight: "200px",
                    }}
                    className="img-fluid rounded-3"
                  />
                  <i
                    onClick={() => fileInputRef.current.click()}
                    className="icofont icofont-ui-camera"
                    style={{ fontSize: "25px", cursor: "pointer" }}
                  ></i>
                </div>
              ) : (
                <></>
              )}

              <div
                className="custom-file-upload"
                style={{ display: previewImage ? "none" : "" }}
              >
                <input
                  type="file"
                  id="myfile"
                  ref={fileInputRef}
                  name="thumbnail"
                  onChange={(e) => handleThumbnailImage(e)}
                  accept=".png, .jpg, .jpeg"
                  className="form-control"
                />
                <div
                  className="upload-div"
                  style={{ height: "200px", width: "200px" }}
                >
                  <div>
                    <img
                      src="/assets/images/upload.svg"
                      height="24"
                      width="24"
                      alt="No image found"
                    />
                  </div>
                  <p>Upload Images of type jpg,jpeg or png upto 10 MB</p>
                </div>
              </div>

              <p className="error-msg">{errors.logo}</p>
            </Col>
            <Col md={6}>
              <FormGroup>
                <Label className="col-form-label">Restaurant Name</Label>
                <Input
                  type="text"
                  placeholder="Name"
                  value={restaurantDetails.fullName}
                  readOnly={true}
                  style={{ cursor: "not-allowed" }}
                  name="fullName"
                  onChange={(e) => handleChange(e)}
                />
                <p className="error-msg">{errors.fullName}</p>
              </FormGroup>
            </Col>

            <Col md={3}>
              <FormGroup>
                <Label className="col-form-label">{EmailAddress}</Label>
                <Input
                  type="email"
                  placeholder="Test@gmail.com"
                  value={restaurantDetails.email}
                  readOnly={true}
                  style={{ cursor: "not-allowed" }}
                  name="email"
                  onChange={(e) => handleChange(e)}
                />
                <p className="error-msg">{errors.email}</p>
              </FormGroup>
            </Col>

            <Col md={3}>
              <FormGroup>
                <Label className="col-form-label">Phone No.</Label>
                <Input
                  type="number"
                  placeholder="Enter Phone number"
                  value={restaurantDetails.phoneNo}
                  name="phoneNo"
                  onChange={(e) => handleChange(e)}
                />
                <p className="error-msg">{errors.phoneNo}</p>
              </FormGroup>
            </Col>

            <Col md={12}>
              <FormGroup>
                <Label className="col-form-label">{Address}</Label>
                <textarea
                  className="form-control"
                  rows={3}
                  placeholder="Enter your Address"
                  value={restaurantDetails.address}
                  name="address"
                  onChange={(e) => handleChange(e)}
                />
                <p className="error-msg">{errors.address}</p>
              </FormGroup>
            </Col>

            {restaurantDetails.country ? (
              <Col md={3}>
                <FormGroup>
                  <Label>{Country}</Label>
                  <input
                    type="text"
                    className="form-control"
                    name="country"
                    placeholder="Country Name"
                    readOnly={true}
                    value={restaurantDetails?.country}
                  />
                </FormGroup>
              </Col>
            ) : (
              <></>
            )}

            {restaurantDetails.state ? (
              <Col md={3}>
                <FormGroup>
                  <Label>{State}</Label>
                  <input
                    type="text"
                    className="form-control"
                    name="state"
                    placeholder="State Name"
                    readOnly={true}
                    value={restaurantDetails?.state}
                  />
                </FormGroup>
              </Col>
            ) : (
              <></>
            )}

            {restaurantDetails.city ? (
              <Col md={3}>
                <FormGroup>
                  <Label>{City}</Label>
                  <input
                    type="text"
                    name="city"
                    className="form-control"
                    placeholder="City Name"
                    readOnly={true}
                    value={restaurantDetails?.city}
                  />
                </FormGroup>
              </Col>
            ) : (
              <></>
            )}

            <Col md={3}>
              <FormGroup>
                <Label>{PostalCode}</Label>
                <input
                  type="number"
                  className="form-control"
                  id="zip"
                  placeholder="Enter Postal Code"
                  value={restaurantDetails?.pinCode}
                  name="pinCode"
                  onChange={(e) => hamdleZipChange(e)}
                />
                <p className="error-msg">{errors.pinCode}</p>
              </FormGroup>
            </Col>
          </Row>

          <CardHeader
            style={{
              border: "1px solid darkgray",
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              marginTop: "15px",
            }}
          >
            <h4 className="card-title mb-0">Review Platforms</h4>
            <i
              style={{ cursor: "pointer" }}
              onClick={() => addPlatforms()}
              className="icofont icofont-ui-add"
            ></i>
          </CardHeader>
          <CardBody
            style={{
              borderLeft: "1px solid darkgray",
              borderRight: "1px solid darkgray",
              borderBottom: "1px solid darkgray",
            }}
          >
            <Row>
              <Col
                md={1}
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  fontWeight: "500",
                }}
              ></Col>
              <Col
                md={3}
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  fontWeight: "500",
                }}
              >
                <span>Review Platform</span>
              </Col>
              <Col
                md={7}
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  fontWeight: "500",
                }}
              >
                <span>Platform Link</span>
              </Col>
              <Col
                md={1}
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  fontWeight: "500",
                }}
              >
                <span>Actions</span>
              </Col>
            </Row>

            {restaurantDetails &&
              restaurantDetails.reviewPlatformLinks.map((item, index) => (
                <>
                  <Row key={index} style={{ padding: "10px" }}>
                    <Col
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                      md={1}
                    >
                      <div className="checkbox-checked">
                        <Input
                          className="form-check-input"
                          type="checkbox"
                          id="flexCheckDefault"
                          onChange={(e) => handlePlatformChange(e, item, index)}
                          name="isSelected"
                          style={{
                            height: "1.5rem",
                            width: "1.5rem",
                            border: "1px solid #7366ff",
                          }}
                          checked={item.isSelected}
                        />
                      </div>
                    </Col>

                    <Col
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                      md={3}
                    >
                      <select
                        value={item.platform}
                        name="platform"
                        onChange={(e) => handlePlatformChange(e, item, index)}
                        className="form-select form-select"
                      >
                        <option selected disabled value="">
                          Select Platform
                        </option>
                        {reviewPlatformList && reviewPlatformList.length ? (
                          reviewPlatformList.map((val, index) => (
                            <option key={index} value={val.id}>{val.title}</option>
                          ))
                        ) : (
                          <></>
                        )}
                      </select>
                    </Col>

                    <Col
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                      md={7}
                    >
                      <Input
                        type="text"
                        placeholder="Type your title in Placeholder"
                        name="link"
                        onChange={(e) => handlePlatformChange(e, item, index)}
                        value={item.link}
                      />
                    </Col>

                    <Col
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                      md={1}
                    >
                      <i
                        style={{ cursor: "pointer" }}
                        onClick={() => deletePlatforms(index)}
                        className="icofont icofont-ui-delete"
                      ></i>
                    </Col>
                  </Row>
                </>
              ))}
          </CardBody>
        </CardBody>
        <CardFooter className="text-end">
          <Button color="primary" type="submit">
            {UpdateProfile}
          </Button>
        </CardFooter>
      </form>
    </Col>
  );
};

export default AddEditRestaurant;
