import RestaurantCardsContainer from "@/components/Restaurants/RestaurantCards";
import { getRestaurant } from "@/redux/reducers/restaurantSlice";
import { useEffect, useState } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import { useDispatch } from "react-redux";

const PendingList = () => {
  const [restaurantsArray, setRestaurantsArray] = useState([]);
  const dispatch = useDispatch();
  // States for pagination
  const [totalData, setTotalData] = useState(0);
  const [currPage, setCurrPage] = useState(1);
  const [noOfRecords, setNoOfRecords] = useState(10);
  const [searchVal, setSearchVal] = useState("");
  const [sorting, setSorting] = useState({
    sortBy: "",
    order: "ASC",
  });
  const [hasMore, setHasMore] = useState(true);

  const fetchRestaurants = async (fromAction?) => {
    let obj = {
      search: searchVal,
      pageRecord: noOfRecords,
      pageNo: fromAction ? 1 : currPage,
      sortBy: "",
      order: sorting.order,
      isApproved: "Pending",
    };

    const res = await dispatch(getRestaurant(obj));

    if (!res.payload) {
      return;
    }
    if (res.payload.status === 200 || res.payload.status === "200") {
      const newRestaurantList = res.payload.records;

      if (newRestaurantList.length === 0) {
        setHasMore(false); // No more data to fetch
        if (currPage == 1) {
          setRestaurantsArray([]);
        }
      } else {
        setRestaurantsArray((prevList) => [
          ...prevList,
          ...res.payload.records,
        ]);
        setTotalData(res.payload.totalCount);
      }
    }
  };

  useEffect(() => {
    fetchRestaurants();
  }, [currPage, noOfRecords, searchVal, sorting]);

  return (
    <>
      {restaurantsArray && restaurantsArray.length === 0 ? (
        <>
          <div className="col-xxl-12 col-xl-12 col-lg-12">
            <div className="no-data-found">
              <img src="/assets/images/nodatafound.svg" alt="No Image found" />
              <h4>No Data Found!</h4>
            </div>
          </div>
        </>
      ) : (
        <>
          <InfiniteScroll
            className="infinite-scroll-component "
            dataLength={restaurantsArray?.length}
            next={() => setCurrPage((prevPage) => prevPage + 1)}
            hasMore={hasMore}
            loader={null}
            endMessage={
              restaurantsArray?.length > 0 && (
                <p style={{ textAlign: "center", marginTop: "1.5rem" }}>
                  <b>Yay! You have seen it all</b>
                </p>
              )
            }
          >
            <RestaurantCardsContainer
              restaurantsArray={restaurantsArray}
              fetchRestaurants={fetchRestaurants}
              setRestaurantsArray={setRestaurantsArray}
            />
          </InfiniteScroll>
        </>
      )}
    </>
  );
};

export default PendingList;
