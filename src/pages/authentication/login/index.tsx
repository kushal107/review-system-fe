import CommonLogo from "@/components/Others/authentication/common/CommonLogo";
import Cookies from "js-cookie";
import Link from "next/link";
import { useRouter } from "next/router";
import { ChangeEvent, FormEvent, useState } from "react";
import { Facebook, Linkedin, Twitter } from "react-feather";
import { toast } from "react-toastify";
import {
  Button,
  Col,
  Container,
  FormGroup,
  Input,
  Label,
  Row,
} from "reactstrap";
import {
  CreateAccount,
  DoNotAccount,
  EmailAddress,
  EnterEmailPasswordLogin,
  FacebookHeading,
  ForgotPassword,
  Password,
  RememberPassword,
  SignIn,
  SignInAccount,
  SignInWith,
  TwitterHeading,
  linkedInHeading,
} from "../../../../utils/Constant";

import { useDispatch, useSelector } from "react-redux";
import { loginInUser } from "@/redux/reducers/authSlice";
import { validateSignIn } from "../../../../utils/validation";

const Login = () => {
  const dispatch = useDispatch();

  const [showPassWord, setShowPassWord] = useState(false);
  const { loading } = useSelector((state: any) => state.user);
  const [signInUser, setSignInUser] = useState({
    email: "",
    password: "",
  });
  const [errors, setErrors]: any = useState({});

  const handleChange = (e) => {
    const { name, value } = e.target;
    setSignInUser({
      ...signInUser,
      [name]: value,
    });
    setErrors((error) => {
      let errorNew = { ...error };
      delete errorNew[name];
      return errorNew;
    });
  };

  const router = useRouter();

  const formSubmitHandle = async (event: FormEvent) => {
    event.preventDefault();
    const error = validateSignIn(signInUser);

    const firstErrorField = Object.keys(errors)[0];
    if (firstErrorField) {
      const errorFieldElement = document.getElementById(firstErrorField)
        ? document.getElementById(firstErrorField)
        : document.getElementsByName(firstErrorField)[0];
      if (errorFieldElement) {
        errorFieldElement.scrollIntoView({
          behavior: "smooth",
          block: "center",
        });
      }
    }
    if (Object.keys(error).length) {
      setErrors(error);
      return;
    }
    const playload = {
      email: signInUser.email,
      password: signInUser.password,
    };
    const signInResponse = await dispatch(loginInUser(playload));
    if (!signInResponse.payload) {
      return;
    }
    if (
      signInResponse.payload.status === 200 ||
      signInResponse.payload.status === "200"
    ) {
      const accessToken = signInResponse.payload.accessToken;
      const refreshToken = signInResponse.payload.refreshToken;
      localStorage.setItem("userRefreshToken", refreshToken);
      localStorage.setItem("userAccess", `Bearer ${accessToken}`);

      Cookies.set("userAccess", JSON.stringify(`Bearer ${accessToken}`));

      router.replace("/dashboard");
      toast.success("Logged in successfully");
      setSignInUser({
        email: "",
        password: "",
      });
    }
  };

  return (
    <Container fluid className="p-0">
      <Row className="m-0">
        <Col xs={12} className="p-0">
          <div className="login-card login-dark">
            <div>
              <div>
                <CommonLogo />
              </div>
              <div className="login-main">
                <form className="theme-form" onSubmit={formSubmitHandle}>
                  <h4>{SignInAccount}</h4>
                  <p>{EnterEmailPasswordLogin}</p>
                  <FormGroup>
                    <Label className="col-form-label">{EmailAddress}</Label>
                    <Input
                      type="email"
                      placeholder="Test@gmail.com"
                      value={signInUser.email}
                      name="email"
                      onChange={(e) => handleChange(e)}
                    />
                    <p className="error-msg">{errors.email}</p>
                  </FormGroup>
                  <FormGroup>
                    <Label className="col-form-label">{Password}</Label>
                    <div className="form-input position-relative">
                      <Input
                        type={showPassWord ? "text" : "password"}
                        placeholder="*********"
                        onChange={(e) => handleChange(e)}
                        value={signInUser.password}
                        name="password"
                      />
                      <div className="show-hide">
                        <span
                          onClick={() => setShowPassWord(!showPassWord)}
                          className={!showPassWord ? "show" : ""}
                        />
                      </div>
                    </div>
                    <p className="error-msg">{errors.password}</p>
                  </FormGroup>
                  <FormGroup className="mb-0 form-group">
                    <Link
                      className="link"
                      href="/pages/authentication/forget-pwd"
                    >
                      {ForgotPassword}
                    </Link>
                    <div className="text-end mt-3">
                      <Button
                        color="primary"
                        className="btn-block w-100"
                        type="submit"
                      >
                        {SignIn}
                      </Button>
                    </div>
                  </FormGroup>

                  <p className="mt-4 mb-0 text-center">
                    {DoNotAccount}
                    <Link className="ms-2" href="/authentication/signup">
                      {CreateAccount}
                    </Link>
                  </p>
                </form>
              </div>
            </div>
          </div>
        </Col>
      </Row>
    </Container>
  );
};

export default Login;
