import { signUpUser } from "@/redux/reducers/authSlice";
import Link from "next/link";
import { useRouter } from "next/router";
import { FormEvent, useState } from "react";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import {
  Button,
  Col,
  Container,
  FormGroup,
  Input,
  Label,
  Row,
} from "reactstrap";
import {
  CreateAccount,
  CreateYourAccount,
  EmailAddress,
  EnterYourPersonalDetails,
  HaveAccount,
  Password,
  SignIn,
} from "../../../../utils/Constant";
import { validateSignUp } from "../../../../utils/validation";
import CommonLogo from "../../../components/Others/authentication/common/CommonLogo";

const Register = () => {
  const dispatch = useDispatch();
  const [showPassWord, setShowPassWord] = useState(false);

  const [signInUser, setSignInUser] = useState({
    fullName: "",
    email: "",
    password: "",
  });
  const [errors, setErrors]: any = useState({});

  const handleChange = (e) => {
    const { name, value } = e.target;
    setSignInUser({
      ...signInUser,
      [name]: value,
    });
    setErrors((error) => {
      let errorNew = { ...error };
      delete errorNew[name];
      return errorNew;
    });
  };

  const router = useRouter();

  const formSubmitHandle = async (event: FormEvent) => {
    event.preventDefault();
    const error = validateSignUp(signInUser);

    const firstErrorField = Object.keys(errors)[0];
    if (firstErrorField) {
      const errorFieldElement = document.getElementById(firstErrorField)
        ? document.getElementById(firstErrorField)
        : document.getElementsByName(firstErrorField)[0];
      if (errorFieldElement) {
        errorFieldElement.scrollIntoView({
          behavior: "smooth",
          block: "center",
        });
      }
    }
    if (Object.keys(error).length) {
      setErrors(error);
      return;
    }
    const playload = {
      fullName: signInUser.fullName,
      email: signInUser.email,
      password: signInUser.password,
    };
    const signInResponse = await dispatch(signUpUser(playload));
    if (!signInResponse.payload) {
      return;
    }
    if (
      signInResponse.payload.status === 200 ||
      signInResponse.payload.status === "200"
    ) {
      router.replace("/authentication/login");
      toast.success("Registration successfull");
      setSignInUser({
        fullName: "",
        email: "",
        password: "",
      });
    } else {
      toast.error(signInResponse.payload.message);
    }
  };

  return (
    <Container fluid className="p-0">
      <Row className="m-0">
        <Col xs={12} className="p-0">
          <div className="login-card login-dark">
            <div>
              <div>
                <CommonLogo />
              </div>
              <div className="login-main">
                <form className="theme-form" onSubmit={formSubmitHandle}>
                  <h4>{CreateYourAccount}</h4>
                  <p>{EnterYourPersonalDetails}</p>
                  <FormGroup>
                    <Label className="col-form-label pt-0">Name</Label>
                    <Input
                      type="text"
                      placeholder="Name"
                      value={signInUser.fullName}
                      name="fullName"
                      onChange={(e) => handleChange(e)}
                    />
                    <p className="error-msg">{errors.fullName}</p>
                  </FormGroup>
                  <FormGroup>
                    <Label className="col-form-label">{EmailAddress}</Label>
                    <Input
                      type="text"
                      placeholder="Enter Email"
                      value={signInUser.email}
                      name="email"
                      onChange={(e) => handleChange(e)}
                    />
                    <p className="error-msg">{errors.email}</p>
                  </FormGroup>
                  <FormGroup>
                    <Label className="col-form-label">{Password}</Label>
                    <div className="form-input position-relative">
                      <Input
                        type={showPassWord ? "text" : "password"}
                        placeholder="*********"
                        onChange={(e) => handleChange(e)}
                        value={signInUser.password}
                        name="password"
                      />

                      <div className="show-hide">
                        <span
                          onClick={() => setShowPassWord(!showPassWord)}
                          className={!showPassWord ? "show" : ""}
                        />
                      </div>
                    </div>
                    <p className="error-msg">{errors.password}</p>
                  </FormGroup>
                  <FormGroup className="mb-0">
                    <Button color="primary" className="btn-block w-100">
                      {CreateAccount}
                    </Button>
                  </FormGroup>

                  <p className="mt-4 mb-0">
                    {HaveAccount}
                    <Link className="ms-2" href="/authentication/login">
                      {SignIn}
                    </Link>
                  </p>
                </form>
              </div>
            </div>
          </div>
        </Col>
      </Row>
    </Container>
  );
};

export default Register;
