import ReviewViewList from "@/components/Reviews";
import { getReview } from "@/redux/reducers/reviewSlice";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import { useDispatch, useSelector } from "react-redux";
import { Card, Col, Row } from "reactstrap";

const ReviewList = ({ restaurantId = null }) => {
  const [reviewsArray, setReviewsArray] = useState([]);
  const dispatch = useDispatch();
  const router = useRouter();
  const user = useSelector((state: any) => state.user?.user);

  // States for pagination
  const [totalData, setTotalData] = useState(0);
  const [currPage, setCurrPage] = useState(1);
  const [noOfRecords, setNoOfRecords] = useState(10);
  const [searchVal, setSearchVal] = useState("");
  const [sorting, setSorting] = useState({
    sortBy: "",
    order: "ASC",
  });
  const [hasMore, setHasMore] = useState(true);

  const fetchReviews = async (fromAction?) => {
    let obj = {
      search: searchVal,
      pageRecord: noOfRecords,
      pageNo: fromAction ? 1 : currPage,
      sortBy: "",
      order: sorting.order,
      restaurantId: router.query.restaurantId,
    };

    const res = await dispatch(getReview(obj));

    if (!res.payload) {
      return;
    }
    if (res.payload.status === 200 || res.payload.status === "200") {
      const newReviewList = res.payload.records;

      if (newReviewList.length === 0) {
        setHasMore(false); // No more data to fetch
        if (currPage == 1) {
          setReviewsArray([]);
        }
      } else {
        setReviewsArray((prevList) => [...prevList, ...res.payload.records]);
        setTotalData(res.payload.totalCount);
      }
    }
  };

  useEffect(() => {
    if (router.query.restaurantId) {
      fetchReviews();
    }
  }, [currPage, noOfRecords, searchVal, sorting, router.query.restaurantId]);

  return (
    <>
      <div className="page-body">
        <Col
          md={12}
          className="project-list"
          style={{
            paddingTop: "15px",
            paddingLeft: "15px",
            paddingRight: "15px",
          }}
        >
          <Card>
            <Row>
              <Col md={6} style={{ display: "flex", alignItems: "center" }}>
                <div
                  style={{
                    fontSize: "18px",
                    color: "#7366FF",
                    fontWeight: "bold",
                  }}
                >
                  <span>
                    Reviews List
                    {reviewsArray && reviewsArray.length ? (
                      <>
                        {" - "}
                        <span>{reviewsArray[0].User?.fullName}</span>
                      </>
                    ) : (
                      <></>
                    )}
                  </span>
                </div>
              </Col>
            </Row>
          </Card>
        </Col>

        {reviewsArray && reviewsArray.length === 0 ? (
          <>
            <div className="col-xxl-12 col-xl-12 col-lg-12">
              <div className="no-data-found">
                <img
                  src="/assets/images/nodatafound.svg"
                  alt="No Image found"
                />
                <h4>No Data Found!</h4>
              </div>
            </div>
          </>
        ) : (
          <>
            <InfiniteScroll
              className="infinite-scroll-component "
              dataLength={reviewsArray?.length}
              next={() => setCurrPage((prevPage) => prevPage + 1)}
              hasMore={hasMore}
              loader={null}
              endMessage={
                reviewsArray?.length > 0 && (
                  <p style={{ textAlign: "center", marginTop: "1.5rem" }}>
                    <b>Yay! You have seen it all</b>
                  </p>
                )
              }
            >
              <ReviewViewList
                reviewsArray={reviewsArray}
                fetchReviews={fetchReviews}
                setReviewsArray={setReviewsArray}
              />
            </InfiniteScroll>
          </>
        )}
      </div>
    </>
  );
};

export default ReviewList;
