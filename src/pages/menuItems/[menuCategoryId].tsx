import MenuItemViewList from "@/components/MenuItems";
import {
  addMenuItem,
  getMenuItem,
  updateMenuItem,
} from "@/redux/reducers/menuItemSlice";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { PlusCircle } from "react-feather";
import InfiniteScroll from "react-infinite-scroll-component";
import { useDispatch, useSelector } from "react-redux";
import {
  Card,
  CardBody,
  Col,
  Container,
  FormGroup,
  Input,
  Row,
  Label,
} from "reactstrap";
import CommonModal from "../../../CommonElements/Ui-kits/CommonModal";
import { toast } from "react-toastify";

const MenuItemList = () => {
  const [menuItemsArray, setMenuItemsArray] = useState([]);
  const dispatch = useDispatch();
  const router = useRouter();
  const user = useSelector((state: any) => state.user?.user);

  // States for pagination
  const [totalData, setTotalData] = useState(0);
  const [currPage, setCurrPage] = useState(1);
  const [noOfRecords, setNoOfRecords] = useState(10);
  const [searchVal, setSearchVal] = useState("");
  const [sorting, setSorting] = useState({
    sortBy: "",
    order: "ASC",
  });
  const [hasMore, setHasMore] = useState(true);

  const fetchMenuItems = async (fromAction?) => {
    let obj = {
      search: searchVal,
      pageRecord: noOfRecords,
      pageNo: fromAction ? 1 : currPage,
      sortBy: "",
      order: sorting.order,
      isApproved: "Active",
      menuCategoryId: router.query.menuCategoryId,
    };

    const res = await dispatch(getMenuItem(obj));

    if (!res.payload) {
      return;
    }
    if (res.payload.status === 200 || res.payload.status === "200") {
      const newMenuItemList = res.payload.records;

      if (newMenuItemList.length === 0) {
        setHasMore(false); // No more data to fetch
        if (currPage == 1) {
          setMenuItemsArray([]);
        }
      } else {
        setMenuItemsArray((prevList) => [...prevList, ...res.payload.records]);
        setTotalData(res.payload.totalCount);
      }
    }
  };

  const [errors, setErrors]: any = useState({});

  const [menuItemDetails, setMenuItemDetails] = useState({
    name: "",
    description: "",
    price: null,
    isVeg: true,
    isJain: false,
    isSpicy: false,
    isHalf: false,
    calories: null,
    categoryId: "",
    id: "",
  });

  const [modal, setModal] = useState<boolean>(false);
  const toggle = () => {
    setModal(!modal);
  };

  let ModalData = {
    isOpen: modal,
    header: true,
    footer: true,
    toggler: toggle,
    class: "modal-dialog-centered",
    title: menuItemDetails.id ? "Edit Menu Item" : "Add Menu Item",
    button: menuItemDetails.id ? "Update" : "Save",
    id: null,
  };

  const handleChange = (e) => {
    const { name, value, checked } = e.target;

    if (
      name == "isVeg" ||
      name == "isJain" ||
      name == "isHalf" ||
      name == "isSpicy"
    ) {
      setMenuItemDetails({
        ...menuItemDetails,
        [name]: checked,
      });
    } else {
      setMenuItemDetails({
        ...menuItemDetails,
        [name]: value,
      });
    }

    setErrors((error) => {
      let errorNew = { ...error };
      delete errorNew[name];
      return errorNew;
    });
  };

  const handleSubmitProfile = async (e?) => {
    let validateData = {
      name: menuItemDetails.name ? menuItemDetails.name : "",
      description: menuItemDetails.description
        ? menuItemDetails.description
        : "",
      price: menuItemDetails.price ? parseInt(menuItemDetails.price) : null,
      isVeg: menuItemDetails.isVeg,
      isJain: menuItemDetails.isJain,
      isSpicy: menuItemDetails.isSpicy,
      isHalf: menuItemDetails.isHalf,
      calories: menuItemDetails.calories
        ? parseInt(menuItemDetails.calories)
        : null,
      categoryId: router.query.menuCategoryId,
    };

    let errors: any = {};

    if (!validateData.name) {
      errors.name = "Name is required";
    }

    if (!validateData.description) {
      errors.description = "Description is required";
    }

    if (!validateData.price) {
      errors.price = "Price is required";
    }

    if (!validateData.calories) {
      errors.calories = "Calories is required";
    }

    const firstErrorField = Object.keys(errors)[0];
    if (firstErrorField) {
      const errorFieldElement = document.getElementById(firstErrorField)
        ? document.getElementById(firstErrorField)
        : document.getElementsByName(firstErrorField)[0];
      if (errorFieldElement) {
        errorFieldElement.scrollIntoView({
          behavior: "smooth",
          block: "center",
        });
      }
    }

    if (Object.keys(errors).length) {
      setErrors(errors);
      return;
    }

    if (Object.keys(errors).length) {
      setErrors(errors);
      return;
    }

    if (menuItemDetails.id) {
      const payload = {
        formData: validateData,
        id: menuItemDetails.id,
      };

      const editCourseRes = await dispatch(updateMenuItem(payload));
      if (!editCourseRes) {
        return;
      }
      if (
        editCourseRes.payload?.status === 200 ||
        editCourseRes.payload?.status === "200"
      ) {
        toggle();
        setMenuItemsArray([]);
        fetchMenuItems(true);
        toast.success("Menu Item updated successfully");
        setMenuItemDetails({
          name: "",
          description: "",
          price: null,
          isVeg: true,
          isJain: false,
          isSpicy: false,
          isHalf: false,
          calories: null,
          categoryId: "",
          id: "",
        });
      }
    } else {
      const payload = {
        formData: validateData,
      };

      const editCourseRes = await dispatch(addMenuItem(payload));
      if (!editCourseRes) {
        return;
      }
      if (
        editCourseRes.payload?.status === 200 ||
        editCourseRes.payload?.status === "200"
      ) {
        toggle();
        setMenuItemsArray([]);
        fetchMenuItems(true);
        toast.success("Menu Item added successfully");
        setMenuItemDetails({
          name: "",
          description: "",
          price: null,
          isVeg: true,
          isJain: false,
          isSpicy: false,
          isHalf: false,
          calories: null,
          categoryId: "",
          id: "",
        });
      }
    }
  };

  useEffect(() => {
    if (router.query.menuCategoryId) {
      fetchMenuItems();
    }
  }, [currPage, noOfRecords, searchVal, sorting, router.query.menuCategoryId]);

  return (
    <>
      <div className="page-body">
        <Col
          md={12}
          className="project-list"
          style={{
            paddingTop: "15px",
            paddingLeft: "15px",
            paddingRight: "15px",
          }}
        >
          <Card>
            <Row>
              <Col md={6} style={{ display: "flex", alignItems: "center" }}>
                <div
                  style={{
                    fontSize: "18px",
                    color: "#7366FF",
                    fontWeight: "bold",
                  }}
                >
                  <strong
                    onClick={router.back}
                    style={{
                      fontSize: "18px",
                      marginRight: "10px",
                      cursor: "pointer",
                    }}
                  >
                    {"<"}
                  </strong>{" "}
                  <span>Items List</span>
                </div>
              </Col>
              <Col md={6}>
                <span
                  className="btn btn-primary"
                  style={{
                    color: "white",
                    display: "flex",
                    alignItems: "center",
                  }}
                  onClick={() => toggle()}
                >
                  <PlusCircle
                    style={{
                      width: "20px",
                      height: "20px",
                      marginRight: "5px",
                    }}
                  />
                  <span>Add Menu Item</span>
                </span>

                {modal && (
                  <CommonModal
                    modalData={ModalData}
                    clickFn={() => {
                      handleSubmitProfile();
                    }}
                  >
                    <div className="modal-toggle-wrapper">
                      <Row>
                        <Col md={12}>
                          <FormGroup>
                            <Label className="col-form-label">Name</Label>
                            <Input
                              type="text"
                              placeholder="Enter Name"
                              value={menuItemDetails.name}
                              name="name"
                              onChange={(e) => handleChange(e)}
                            />
                            <p className="error-msg">{errors.name}</p>
                          </FormGroup>
                        </Col>

                        <Col md={12}>
                          <FormGroup>
                            <Label className="col-form-label">
                              Description
                            </Label>
                            <textarea
                              placeholder="Enter Description"
                              value={menuItemDetails.description}
                              className="form-control"
                              name="description"
                              rows={5}
                              onChange={(e) => handleChange(e)}
                            />
                            <p className="error-msg">{errors.description}</p>
                          </FormGroup>
                        </Col>

                        <Col md={6}>
                          <FormGroup>
                            <Label className="col-form-label">Price</Label>
                            <Input
                              type="number"
                              placeholder="Enter Price"
                              value={menuItemDetails.price}
                              name="price"
                              onChange={(e) => handleChange(e)}
                            />
                            <p className="error-msg">{errors.price}</p>
                          </FormGroup>
                        </Col>

                        <Col md={6}>
                          <FormGroup>
                            <Label className="col-form-label">Calories</Label>
                            <Input
                              type="number"
                              placeholder="Enter Calories"
                              value={menuItemDetails.calories}
                              name="calories"
                              onChange={(e) => handleChange(e)}
                            />
                            <p className="error-msg">{errors.calories}</p>
                          </FormGroup>
                        </Col>

                        <Col md={3}>
                          <FormGroup
                            switch
                            check
                            style={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                              paddingLeft: "0",
                            }}
                          >
                            <Label className="col-form-label">Veg</Label>
                            <Input
                              id="isVeg"
                              name="isVeg"
                              onChange={(e) => handleChange(e)}
                              type="switch"
                              checked={menuItemDetails.isVeg}
                              style={{
                                fontSize: "18px",
                                margin: "0 0 0 5px",
                              }}
                            />
                          </FormGroup>
                        </Col>

                        <Col md={3}>
                          <FormGroup
                            switch
                            check
                            style={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                              paddingLeft: "0",
                            }}
                          >
                            <Label className="col-form-label">Jain</Label>
                            <Input
                              id="isJain"
                              name="isJain"
                              onChange={(e) => handleChange(e)}
                              type="switch"
                              checked={menuItemDetails.isJain}
                              style={{
                                fontSize: "18px",
                                margin: "0 0 0 5px",
                              }}
                            />
                          </FormGroup>
                        </Col>

                        <Col md={3}>
                          <FormGroup
                            switch
                            check
                            style={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                              paddingLeft: "0",
                            }}
                          >
                            <Label className="col-form-label">Spicy</Label>
                            <Input
                              id="isSpicy"
                              name="isSpicy"
                              onChange={(e) => handleChange(e)}
                              type="switch"
                              checked={menuItemDetails.isSpicy}
                              style={{
                                fontSize: "18px",
                                margin: "0 0 0 5px",
                              }}
                            />
                          </FormGroup>
                        </Col>

                        <Col md={3}>
                          <FormGroup
                            switch
                            check
                            style={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                              paddingLeft: "0",
                            }}
                          >
                            <Label className="col-form-label">Half</Label>
                            <Input
                              id="isHalf"
                              name="isHalf"
                              onChange={(e) => handleChange(e)}
                              type="switch"
                              checked={menuItemDetails.isHalf}
                              style={{
                                fontSize: "18px",
                                margin: "0 0 0 5px",
                              }}
                            />
                          </FormGroup>
                        </Col>
                      </Row>
                    </div>
                  </CommonModal>
                )}
              </Col>
            </Row>
          </Card>
        </Col>

        {menuItemsArray && menuItemsArray.length === 0 ? (
          <>
            <div className="col-xxl-12 col-xl-12 col-lg-12">
              <div className="no-data-found">
                <img
                  src="/assets/images/nodatafound.svg"
                  alt="No Image found"
                />
                <h4>No Data Found!</h4>
              </div>
            </div>
          </>
        ) : (
          <>
            <InfiniteScroll
              className="infinite-scroll-component "
              dataLength={menuItemsArray?.length}
              next={() => setCurrPage((prevPage) => prevPage + 1)}
              hasMore={hasMore}
              loader={null}
              endMessage={
                menuItemsArray?.length > 0 && (
                  <p style={{ textAlign: "center", marginTop: "1.5rem" }}>
                    <b>Yay! You have seen it all</b>
                  </p>
                )
              }
            >
              <MenuItemViewList
                menuItemsArray={menuItemsArray}
                fetchMenuItems={fetchMenuItems}
                setMenuItemsArray={setMenuItemsArray}
                menuCategoryId={router.query.menuCategoryId}
                setMenuItemDetails={setMenuItemDetails}
                toggleOutside={toggle}
              />
            </InfiniteScroll>
          </>
        )}
      </div>
    </>
  );
};

export default MenuItemList;
