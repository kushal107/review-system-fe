import StaffCardsContainer from "@/components/StaffCards";
import Link from "next/link";
import { PlusCircle } from "react-feather";
import { Card, Col, Row } from "reactstrap";

const StaffCard = () => {
  return (
    <div className="page-body">
      {/* <Breadcrumbs title={StaffList} mainTitle={StaffList} /> */}
      <Col md={12} className="project-list" style={{ paddingTop: "15px" }}>
        <Card>
          <Row>
            <Col md={6} style={{ display: "flex", alignItems: "center" }}>
              <div
                style={{
                  fontSize: "18px",
                  color: "#7366FF",
                  fontWeight: "bold",
                }}
              >
                <span>Staff List</span>
              </div>
            </Col>
            <Col md={6}>
              <div className="text-end">
                <Link
                  className="btn btn-primary"
                  style={{ color: "white" }}
                  href={`#`}
                >
                  <PlusCircle
                    style={{
                      width: "20px",
                      height: "20px",
                      marginRight: "5px",
                    }}
                  />
                  <span>Add Staff</span>
                </Link>
              </div>
            </Col>
          </Row>
        </Card>
      </Col>
      <StaffCardsContainer />
    </div>
  );
};

export default StaffCard;
