"use client";

import store from "@/redux/store";
import { useRouter } from "next/router";
import { Provider } from "react-redux";
import { ToastContainer } from "react-toastify";
import { withoutLayoutThemePath } from "../../Data/OthersPageData";
import { CustomizerProvider } from "../../helper/Customizer/CustomizerProvider";
import LayoutProvider from "../../helper/Layout/LayoutProvider";
import "../../public/assets/scss/app.scss";
import Layout from "../layout";

const Myapp = ({ Component, pageProps }: any) => {
  const getLayout =
    Component.getLayout || ((page: any) => <Layout>{page}</Layout>);
  const router = useRouter();
  const currentUrl = router.asPath;
  let updatedPath;
  if (currentUrl.includes("?")) {
    const tempt = currentUrl;
    updatedPath = tempt.split("?")[0];
  } else {
    updatedPath = currentUrl;
  }

  // console.log("first")

  return (
    <>
      <Provider store={store}>
        {withoutLayoutThemePath.includes(updatedPath) ? (
          <Component {...pageProps} />
        ) : (
          <CustomizerProvider>
            <LayoutProvider>
              {getLayout(<Component {...pageProps} />)}
            </LayoutProvider>
          </CustomizerProvider>
        )}
      </Provider>
      <ToastContainer />
    </>
  );
};

export default Myapp;
