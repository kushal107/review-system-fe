import CustomerCardsContainer from "@/components/CustomerCards";
import { Card, Col, Row } from "reactstrap";

const CustomerCard = () => {
  return (
    <div className="page-body">
      <Col md={12} className="project-list" style={{ paddingTop: "15px" }}>
        <Card>
          <Row>
            <Col md={6} style={{ display: "flex", alignItems: "center" }}>
              <div
                style={{
                  fontSize: "18px",
                  color: "#7366FF",
                  fontWeight: "bold",
                }}
              >
                <span>Customer List</span>
              </div>
            </Col>
            <Col md={6}>
             
            </Col>
          </Row>
        </Card>
      </Col>
      <CustomerCardsContainer />
    </div>
  );
};

export default CustomerCard;
