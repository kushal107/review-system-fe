import AddEditRestaurant from "@/components/Restaurants/RestaurantAddEdit";

const StaffCard = () => {
  return (
    <div className="page-body">
      <AddEditRestaurant></AddEditRestaurant>
    </div>
  );
};

export default StaffCard;
