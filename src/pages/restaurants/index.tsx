import ActiveList from "@/components/Restaurants/Listing/ActiveList";
import InActiveList from "@/components/Restaurants/Listing/InActiveList";
import PendingList from "@/components/Restaurants/Listing/PendingList";
import { useState } from "react";
import { CheckCircle, Info, Target } from "react-feather";
import { Card, Col, Nav, NavItem, NavLink, Row } from "reactstrap";

const RestaurantCard = () => {
  const [activeTab, setActiveTab] = useState("Active");

  return (
    <div className="page-body">
      <Col
        md={12}
        className="project-list"
        style={{
          paddingTop: "15px",
          paddingLeft: "15px",
          paddingRight: "15px",
        }}
      >
        <Card>
          <Row>
            <Col md={6} style={{ display: "flex", alignItems: "center" }}>
              <div
                style={{
                  fontSize: "18px",
                  color: "#7366FF",
                  fontWeight: "bold",
                }}
              >
                <span>Restaurant List</span>
              </div>
            </Col>
            <Col md={6} style={{ display: "flex", justifyContent: "end" }}>
              <Nav tabs className="border-tab">
                <NavItem>
                  <NavLink
                    className={activeTab === "Active" ? "active" : ""}
                    onClick={() => setActiveTab("Active")}
                  >
                    <Target />
                    Active
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={activeTab === "InActive" ? "active" : ""}
                    onClick={() => setActiveTab("InActive")}
                  >
                    <Info />
                    InActive
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={activeTab === "Pending" ? "active" : ""}
                    onClick={() => setActiveTab("Pending")}
                  >
                    {" "}
                    <CheckCircle /> Pending
                  </NavLink>
                </NavItem>
              </Nav>
            </Col>
          </Row>
        </Card>
      </Col>

      <Col>
        {activeTab == "Active" ? <ActiveList /> : <></>}
        {activeTab == "InActive" ? <InActiveList /> : <></>}
        {activeTab == "Pending" ? <PendingList /> : <></>}
      </Col>
    </div>
  );
};

export default RestaurantCard;
