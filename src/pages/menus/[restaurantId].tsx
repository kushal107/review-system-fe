import ActiveList from "@/components/Menus/Listing/ActiveList";
import InActiveList from "@/components/Menus/Listing/InActiveList";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { CheckCircle, Info, Target } from "react-feather";
import { Card, Col, Nav, NavItem, NavLink, Row } from "reactstrap";

const MenuMainListing = () => {
  const [activeTab, setActiveTab] = useState("Active");
  const router = useRouter();

  const [restaurantId, setRestaurantId] = useState(null);
  useEffect(() => {
    if (router.query.restaurantId) {
      setRestaurantId(router.query.restaurantId);
    }
  }, [router.query.restaurantId]);

  return (
    <div className="page-body">
      <Col
        md={12}
        className="project-list"
        style={{
          paddingTop: "15px",
          paddingLeft: "15px",
          paddingRight: "15px",
        }}
      >
        <Card>
          <Row>
            <Col md={6} style={{ display: "flex", alignItems: "center" }}>
              <div
                style={{
                  fontSize: "18px",
                  color: "#7366FF",
                  fontWeight: "bold",
                }}
              >
                <span>Menu Categories</span>
              </div>
            </Col>
            <Col md={6} style={{ display: "flex", justifyContent: "end" }}>
              <Nav tabs className="border-tab">
                <NavItem>
                  <NavLink
                    className={activeTab === "Active" ? "active" : ""}
                    onClick={() => setActiveTab("Active")}
                  >
                    <Target />
                    Active
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={activeTab === "InActive" ? "active" : ""}
                    onClick={() => setActiveTab("InActive")}
                  >
                    <Info />
                    InActive
                  </NavLink>
                </NavItem>
              </Nav>
            </Col>
          </Row>
        </Card>
      </Col>

      <Col>
        {activeTab == "Active" && restaurantId ? (
          <ActiveList restaurantId={restaurantId} />
        ) : (
          <></>
        )}
        {activeTab == "InActive" && restaurantId ? (
          <InActiveList restaurantId={restaurantId} />
        ) : (
          <></>
        )}
      </Col>
    </div>
  );
};

export default MenuMainListing;
