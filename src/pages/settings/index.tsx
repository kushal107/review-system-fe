import SettingTabs from "@/components/SettingTabs";
import { Container, Row } from "reactstrap";

const SettingsPage = () => {
  return (
    <div className="page-body">
      <Container fluid={true}>
        <Row>
          <SettingTabs />
        </Row>
      </Container>
    </div>
  );
};

export default SettingsPage;
