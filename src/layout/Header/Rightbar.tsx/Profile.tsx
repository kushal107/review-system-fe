import { logoutAuth } from "@/redux/reducers/authSlice";
import Cookies from "js-cookie";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import FeatherIconCom from "../../../../CommonElements/Icons/FeatherIconCom";
import { Logout } from "../../../../utils/Constant/index";
import { removeToken } from "../../../../utils/localstorage";

const Profile = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const user = useSelector((state: any) => state?.user?.user);
  const handleLogOut = () => {
    removeToken();
    dispatch(logoutAuth());
    Cookies.remove("userAccess");
    router.push("/authentication/login");
  };
  return (
    <li className="profile-nav onhover-dropdown pe-0 py-0">
      <div className="media profile-media">
        <Image
          className="b-r-10"
          src={
            user && user?.RestaurantProfile && user?.RestaurantProfile?.logo
              ? user?.RestaurantProfile?.logo
              : "/assets/images/avatarForUser.png"
          }
          alt=""
          width={35}
          height={35}
        />
        <div className="media-body">
          <span>{user?.fullName}</span>
          <p className="mb-0 font-roboto">
            {user?.role} <i className="middle fa fa-angle-down" />
          </p>
        </div>
      </div>
      <ul className="profile-dropdown onhover-show-div">
        {user?.role == "Restaurant" ? (
          <li>
            <Link href="/restaurants/profile">
              <FeatherIconCom iconName={"User"} />
              <span>Profile</span>
            </Link>
          </li>
        ) : (
          <></>
        )}
        {user?.role == "SuperAdmin" ? (
          <li>
            <Link href="/settings">
              <FeatherIconCom iconName={"Settings"} />
              <span>Settings</span>
            </Link>
          </li>
        ) : (
          <></>
        )}
        <li onClick={handleLogOut}>
          <a href="#123">
            <FeatherIconCom iconName={"LogIn"} />
            <span>{Logout}</span>
          </a>
        </li>
      </ul>
    </li>
  );
};

export default Profile;
