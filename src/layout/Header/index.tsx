import { useContext, useEffect } from "react";
import { Row } from "reactstrap";
import layoutContext from "../../../helper/Layout";
import Leftbar from "./Leftbar";
import Rightbar from "./Rightbar.tsx";
import { useDispatch, useSelector } from "react-redux";
import { fetchUser } from "@/redux/reducers/authSlice";

const Header = () => {
  const { sideBarToggle } = useContext(layoutContext);

  
  return (
    <div className={`page-header ${sideBarToggle ? "close_icon" : ""}`}>
      <Row className="header-wrapper m-0">
        <Leftbar />
        <Rightbar />
      </Row>
    </div>
  );
};

export default Header;
