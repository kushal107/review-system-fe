import Head from "next/head";
import { ReactNode, useContext, useEffect } from "react";
import Footer from "../../CommonElements/Footer";
import CustomizerContext from "../../helper/Customizer";
import layoutContext, { searchableMenuType } from "../../helper/Layout";
import Header from "./Header";
import Sidebar from "./Sidebar";
import { MenuList } from "./Sidebar/menu";
import Loader from "./loader";
import { useDispatch, useSelector } from "react-redux";
import { fetchUser, setSignleUser } from "@/redux/reducers/authSlice";

interface layoutProps {
  children: ReactNode;
}

const Layout = ({ children }: layoutProps) => {
  const { layout, setLayout } = useContext(CustomizerContext);
  const {
    sideBarToggle,
    setSideBarToggle,
    setSearchableMenu,
    setBookmarkList,
  } = useContext(layoutContext);

  const compactSidebar = () => {
    if (layout === "compact-wrapper") {
      if (window.innerWidth <= 991) {
        setSideBarToggle(true);
      } else {
        setSideBarToggle(false);
      }
    } else if (layout === "horizontal-wrapper") {
      if (window.innerWidth <= 991) {
        setSideBarToggle(true);
        setLayout("compact-wrapper");
      } else {
        setSideBarToggle(false);
        setLayout("horizontal-wrapper");
      }
    }
  };

  useEffect(() => {
    compactSidebar();
    window.addEventListener("resize", () => {
      compactSidebar();
    });
  }, [layout]);

  useEffect(() => {
    const suggestionArray: searchableMenuType[] = [];
    const bookmarkArray: searchableMenuType[] = [];
    let num = 0;

    const getAllLink = (item, icon: ReactNode) => {
      if (item.children) {
        item.children.map((ele) => {
          getAllLink(ele, icon);
        });
      } else {
        num = num + 1;
        suggestionArray.push({
          icon: icon,
          title: item.title ? item.title : "",
          path: item.path ? item.path : "",
          bookmarked: item.bookmark ? item.bookmark : false,
          id: num,
        });
        if (item.bookmark) {
          bookmarkArray.push({
            icon: icon,
            title: item.title ? item.title : "",
            path: item.path ? item.path : "",
            bookmarked: item.bookmark,
            id: num,
          });
        }
      }
    };

    MenuList.forEach((item) => {
      item.Items?.map((child) => {
        getAllLink(child, child.icon);
      });
    });
    setSearchableMenu(suggestionArray);
    setBookmarkList(bookmarkArray);
  }, []);

  const dispatch = useDispatch();
  const { user } = useSelector((state: any) => state.user);

  const getLoggedInUser = async () => {
    const signInUserResponse = await dispatch(fetchUser());
    if (!signInUserResponse.payload) {
      return;
    }
    if (
      signInUserResponse.payload.status === 200 ||
      signInUserResponse.payload.status === "200"
    ) {
      setSignleUser(signInUserResponse.payload);
    }
  };

  useEffect(() => {
    getLoggedInUser();
  }, []);

  return (
    <>
      <Head>
        <title>Cuba - Premium Admin Template</title>
      </Head>
      <Loader />
      <div
        className={`page-wrapper ${sideBarToggle ? "compact-wrapper" : layout}`}
      >
        <Header />
        <div className="page-body-wrapper">
          <Sidebar />
          {children}
          <Footer />
        </div>
      </div>
    </>
  );
};

export default Layout;
