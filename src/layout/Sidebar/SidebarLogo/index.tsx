import Image from "next/image";
import Link from "next/link";

const SidebarLogo = () => {
  return (
    <div className="logo-wrapper">
      <Link href={"/dashboard"} className="d-inline-block">

        <h3>Food Genie</h3>
        {/* <Image
          className="img-fluid for-light"
          src={"/assets/images/logo/logo.png"}
          alt="icon"
          width={121}
          height={100}
        /> */}
        {/* <Image
          className="img-fluid for-dark"
          src={"/assets/images/logo/logo_dark.png"}
          alt="icon"
          width={121}
          height={100}
        /> */}
      </Link>
    </div>
  );
};

export default SidebarLogo;
